<?php

namespace App\Http\Controllers\ScholarshipProvider;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    //
    public function index()
    {
        return view('scholarshipprovider.home');
    }

    

    public function Shortlised(Request $request, $id)
    {
    	
    	//$scholarships=DB::table('scholarship_user')->where('id','23')->update(['status'=>'Shortlised']);
    	$scholarships=DB::table('scholarship_user')->where('id',$id)->update(['status'=>'Shortlised']);

    	return redirect()->back()->with('message','Scholarship Status Successfully Updated');
    }

     public function Rejected(Request $request, $id)
    {
    	
    	//$scholarships=DB::table('scholarship_user')->where('id','23')->update(['status'=>'Shortlised']);
    	$scholarships=DB::table('scholarship_user')->where('id',$id)->update(['status'=>'Rejected']);

    	return redirect()->back()->with('message','Scholarship Status Successfully Updated');
    }

     public function Awarded(Request $request, $id)
    {
    	
    	//$scholarships=DB::table('scholarship_user')->where('id','23')->update(['status'=>'Shortlised']);
    	$scholarships=DB::table('scholarship_user')->where('id',$id)->update(['status'=>'Awarded']);

    	return redirect()->back()->with('message','Scholarship Status Successfully Updated');
    }
    
    
}
