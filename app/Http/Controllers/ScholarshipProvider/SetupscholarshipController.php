<?php

namespace App\Http\Controllers\ScholarshipProvider;

use App\Category;
use App\Course;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroySetupscholarshipRequest;
use App\Http\Requests\StoreSetupscholarshipRequest;
use App\Http\Requests\UpdateSetupscholarshipRequest;
use App\ScholarshipProvider;
use App\Setupscholarship;
use App\Scholarship;
use App\StudentProfile;
use DB;
use App\StudentCourses;
class SetupscholarshipController extends Controller
{
    //

    public function index()
    {
    	$setupscholarships = Scholarship::where('user_id',auth()->user()->id)->get();

        return view('scholarshipprovider.setupscholarships.index', compact('setupscholarships'));
    }

    public function listscheme()
    {
        $listschemes = Scholarship::where('user_id',auth()->user()->id)->get();
        return view('scholarshipprovider.applications.listschemes',compact('listschemes'));
    }

    public function create()
    {
    	$company_names = ScholarshipProvider::all()->pluck('organization_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $categories = Category::all()->pluck('category_name', 'id')->prepend(trans('global.pleaseSelect'), '');

       
         $courses = StudentCourses::all()->pluck('course_name', 'id');

        return view('scholarshipprovider.setupscholarships.create', compact('company_names', 'categories', 'courses'));
    }
    public function store(StoreSetupscholarshipRequest $request)
    {
        $setupscholarship = Scholarship::create($request->all());
        $setupscholarship->courses()->sync($request->input('courses', []));

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $setupscholarship->id]);
        }

        return redirect()->route('setup');
    }

    public function edit(Setupscholarship $setupscholarship,$id)
    {
        
    	$setupscholarship=Scholarship::find($id);
        $company_names = ScholarshipProvider::all()->pluck('organization_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $categories = Category::all()->pluck('category_name', 'id')->prepend(trans('global.pleaseSelect'), '');

       
         $courses = StudentCourses::all()->pluck('course_name', 'id');

        $setupscholarship->load('company_name', 'category', 'courses');

        return view('scholarshipprovider.setupscholarships.edit', compact('company_names', 'categories', 'courses', 'setupscholarship'));
    }

    public function update(UpdateSetupscholarshipRequest $request, Setupscholarship $setupscholarship,$id)

    {
    	$setupscholarship=Scholarship::find($id);
        $setupscholarship->update($request->all());
        $setupscholarship->courses()->sync($request->input('courses', []));

        return redirect()->route('setup');
    }

    public function show(Setupscholarship $scholarships,$id)
    {
        
    	$scholarships=Scholarship::find($id);
        $studentcourse=DB::table('course_scholarship')->where('scholarship_id',$id)->get();
        $scholarships->load('company_name', 'category', 'courses');

        return view('scholarshipprovider.setupscholarships.show', compact('scholarships','studentcourse'));
    }

    public function destroy(Setupscholarship $setupscholarship,$id)
    {
        
    	$setupscholarship=Scholarship::find($id);
        $setupscholarship->delete();

        return back();
    }

    public function massDestroy(MassDestroySetupscholarshipRequest $request)
    {
        Setupscholarship::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        

        $model         = new Setupscholarship();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }

    public function showprofile( StudentProfile $profile,$id)
    {
        $profile=StudentProfile::where('user_id',$id)->first();
        //$profile= \DB::table('student_profiles')->where('user_id',$id)->first();
        return view('scholarshipprovider.applications.showprofile',compact('profile'));
    }

       public function showapplications(Setupscholarship $scholarships,$id)
    {
       global $male,$female,$handicapped,$single_parent ;
        $male=0;$female=0;
        $handicapped=0;
        $single_parent=0;

       $scholarship=Scholarship::find($id);
       $results=DB::table('scholarship_user')->where('scholarship_id',$id)->get();
       $awarded=DB::table('scholarship_user')->where('scholarship_id',$id)->where('status','Awarded')
       ->get();
       $shortlist=DB::table('scholarship_user')->where('scholarship_id',$id)->where('status','Shortlised')
       ->get();

       foreach ($results as $result) {
            # code...
            
            if(StudentProfile::where('user_id',$result->user_id)->where('gender','male')->exists())
            {
                  $profiles=StudentProfile::where('user_id',$result->user_id)->where('gender','male')->first();
             
                   $male=$male+1;
            }
            elseif (StudentProfile::where('user_id',$result->user_id)->where('gender','female')->exists()) {
                # code...
                 $female=$female+1;
            }

            if(StudentProfile::where('user_id',$result->user_id)->where('handicapped','yes')->exists())
            {
                $profiles=StudentProfile::where('user_id',$result->user_id)->where('handicapped','yes')->first();
                $handicapped=$handicapped+1;
            }

            if (StudentProfile::where('user_id',$result->user_id)->where('single_parent','Yes')->exists()) {
                # code...
                $single_parent=$single_parent+1;
            }
          
        }
       return view('scholarshipprovider.applications.applicants',compact('results','scholarship','awarded','shortlist','male','female','handicapped','single_parent')); 
    }

    public function filteredview(Setupscholarship $scholarships,$id)
    {
        $scholarship=Scholarship::find($id);
        $results=DB::table('scholarship_user')->where('scholarship_id',$id)->get();

        
       
        return view('scholarshipprovider.applications.filteredview',compact('results','scholarship')); 
    }

    public function promotion()
    {
         return view('scholarshipprovider.promotion.index');
    }
}
