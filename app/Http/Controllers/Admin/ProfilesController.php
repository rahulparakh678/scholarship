<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyProfileRequest;
use App\Http\Requests\StoreProfileRequest;
use App\Http\Requests\UpdateStudentProfileRequest;
use App\Profile;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\StudentProfile;
use App\Caste;
use App\Course;
use App\Coursetype;
use App\StudStatus;
use Redirect;
use Storage;
use Mail;
use App\User;
use App\StudentCourses;
use DB;
class ProfilesController extends Controller
{
    public function index(Request $request)
    {
        abort_if(Gate::denies('profile_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $annual_income=request('annual_income');

        if ($annual_income) {
            # code...
            $profiles=StudentProfile::where('annual_income','<=',$annual_income)->get();
            return view('admin.profiles.index', compact('profiles'));

        }
        else{
            //$profiles = DB::table('student_profiles')->select('id','fullname','email','mobile','permanent_state','course_type_id','student_course_name_id','ref_code','paid')->orderBy('id','desc')->get()->chunk(12);

            $profiles = StudentProfile::all()->chunk(12);
            return view('admin.profiles.index', compact('profiles'));
        }
        
    }

    public function filterview(Request $request)
    {
        abort_if(Gate::denies('profile_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
         
        
            $profiles = StudentProfile::all()->chunk(12);

            return view('admin.profiles.filterview', compact('profiles'));
        


    }

    public function paid(Request $request,$id)
    {
        $profile=StudentProfile::where('id',$id)->first();
        $profile->paid='YES';
        $profile->save();
         return redirect()->back()->with('message',' Details Saved Successfully');


    }
     public function sfcstu(Request $request,$id)
    {
        $profile=StudentProfile::where('id',$id)->first();
        $profile->paid='SFC';
        $profile->save();
         return redirect()->back()->with('message',' Details Saved Successfully');


    }

    public function create()
    {
        abort_if(Gate::denies('profile_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        //return view('admin.profiles.create');
        $castes = Caste::all()->pluck('caste_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $course_types = Coursetype::all()->pluck('course_type', 'id')->prepend(trans('Please select course type'), '');

        $course_names = Course::all()->pluck('course_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $course=StudentCourses::all()->pluck('course_name', 'id')->prepend(trans('global.pleaseSelect'), '');
        return view('students.profile.create',compact('castes','course_types','course_names','course'));
    }

    public function store(StoreProfileRequest $request)
    {
       $studentDetail = StudentProfile::create($request->all());
        

        return view('admin.profiles.index')->with('message','Profile is completed successfully Now Upload documents in documents Section');
    }

    public function edit(StudentProfile $profile,$id)
    {
         //return view('admin.profiles.create');
       $profile=StudentProfile::where('user_id',$id)->first();
        $castes = Caste::all()->pluck('caste_name', 'id')->prepend(trans('global.pleaseSelect'), '');

       $course_types = Coursetype::all()->pluck('course_type_name', 'id')->prepend(trans('Please select course type'), '');

        $course = Course::all()->pluck('course_name', 'id')->prepend(trans('global.pleaseSelect'), '');

         
        return view('admin.profiles.edit', compact('profile','castes','course_types','course'));
       
        
    }

    public function updatedetails(Request $request,$id)
    {
        
            //$personaldetails=StudentProfile::create($request->all());
            $personaldetails=StudentProfile::where('user_id',$id)->first();
            $personaldetails->update($request->all());
            return redirect()->back()->with('message',' Details Saved Successfully');
        
       
        
    }
    public function show(StudentProfile $profile)
    {
        abort_if(Gate::denies('profile_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $referral=User::where('id',$profile->user_id)->first();
        return view('admin.profiles.show', compact('profile','referral'));
    }

    public function destroy(StudentProfile $profile)
    {
        abort_if(Gate::denies('profile_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $profile->delete();

        return back();
    }

    public function massDestroy(MassDestroyProfileRequest $request)
    {
        Profile::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function scholarshipstatus()
    {
        return view('admin.profiles.studstatus');
    }

    public function storestatus(Request $request)
    {
        //$status=StudStatus::create($request->all());
        
        //return Redirect::to('admin.profiles.index')->with('message','Student Scholarship  Status Updated Successfully');
        $studstatus=new StudStatus;
        $studstatus->user_id=$request->user_id;
        $studstatus->scheme_name=$request->scheme_name;
        $studstatus->status=$request->status;
         if($request->hasFile('applicationpdf'))
        {
            $status=$request->file('applicationpdf')->store('status','s3');
            Storage::disk('s3')->setVisibility($status,'public');
            $url=Storage::disk('s3')->url($status);
            $studstatus->applicationpdf=$url;


        }
        $studstatus->save();
        
        
        return redirect()->route('admin.profiles.index')->with('message','Student Scholarship  Status Added Successfully');
    }
    public function updatestatus(Request $request,$id)
    {
        $scholarships=StudStatus::where('id',$id)->first();
        
        //$scholarships->update($request->all());

        $scholarships->user_id=$request->user_id;
        $scholarships->scheme_name=$request->scheme_name;
        $scholarships->status=$request->status;
         if($request->hasFile('applicationpdf'))
        {
            $status=$request->file('applicationpdf')->store('status','s3');
            Storage::disk('s3')->setVisibility($status,'public');
            $url=Storage::disk('s3')->url($status);
            $scholarships->applicationpdf=$url;


        }
        $scholarships->save();
        
        //return Redirect::to('admin.profiles.index')->with('message','Student Scholarship  Status Updated Successfully');
        return redirect()->route('admin.profiles.index')->with('message','Student Scholarship  Status Updated Successfully');
    }
    public function editscholarshipstatus()
    {
       
       $profiles=StudentProfile::all();
        
        return view('admin.profiles.editstudstaus',compact('profiles'));
    }

    public function updatescholarshipstatus(StudStatus $status,$id)
    {

        $scholarships=StudStatus::where('id',$id)->first();
        return view('admin.profiles.updatestatus',compact('scholarships'));
    }

    public function searchstatus(Request $request)
    {
        $user_id=$request->input('user_id');
        if (StudStatus::where('user_id',$user_id)->exists()) {

            $studstatus=StudStatus::where('user_id',$user_id)->get();
           
            //return redirect()->back()->with('message','User Found Successfully',compact('studstatus'));
            return view('admin.profiles.editstatus',compact('studstatus'));

        }
        else
        {
            
            return redirect()->back()->with('message','User Not  Found ',compact('user_id'));
        }
        
    }

    public function allstatus()
    {   
        $statuses=StudStatus::all();
        return view('admin.profiles.allstatus',compact('statuses'));
    }

}