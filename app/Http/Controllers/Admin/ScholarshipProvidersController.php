<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyScholarshipProviderRequest;
use App\Http\Requests\StoreScholarshipProviderRequest;
use App\Http\Requests\UpdateScholarshipProviderRequest;
use App\ScholarshipProvider;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\User;
use Illuminate\Support\Facades\Hash;

class ScholarshipProvidersController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('scholarship_provider_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $scholarshipProviders = ScholarshipProvider::all();

        return view('admin.scholarshipProviders.index', compact('scholarshipProviders'));
    }

    public function create()
    {
        abort_if(Gate::denies('scholarship_provider_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.scholarshipProviders.create');
    }

     public function sfccreate()
    {
        

        return view('admin.scholarshipProviders.sfccreate');
    }


    public function store(StoreScholarshipProviderRequest $request)
    {
        $scholarshipProvider = ScholarshipProvider::create($request->all());

        User::create([
            'name'=>request('organization_name'),
            'email'=>request('email'),
            'user_type'=>'provider',

            'password'=>bcrypt('password'),
        ]);

        return redirect()->route('admin.scholarship-providers.index');
    }

    public function sfcstore(StoreScholarshipProviderRequest $request)
    {
        $scholarshipProvider = ScholarshipProvider::create($request->all());

        User::create([
            'name'=>request('organization_name'),
            'email'=>request('email'),
            'user_type'=>'sfcngo',
            'ref_code'=>request('ref_code'),
            'password'=>bcrypt('password'),
        ]);

        return redirect()->route('admin.scholarship-providers.index');
    }

    public function edit(ScholarshipProvider $scholarshipProvider)
    {
        abort_if(Gate::denies('scholarship_provider_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.scholarshipProviders.edit', compact('scholarshipProvider'));
    }

    public function update(UpdateScholarshipProviderRequest $request, ScholarshipProvider $scholarshipProvider)
    {
        $scholarshipProvider->update($request->all());

        return redirect()->route('admin.scholarship-providers.index');
    }

    public function show(ScholarshipProvider $scholarshipProvider)
    {
        abort_if(Gate::denies('scholarship_provider_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.scholarshipProviders.show', compact('scholarshipProvider'));
    }

    public function destroy(ScholarshipProvider $scholarshipProvider)
    {
        abort_if(Gate::denies('scholarship_provider_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $scholarshipProvider->delete();

        return back();
    }

    public function massDestroy(MassDestroyScholarshipProviderRequest $request)
    {
        ScholarshipProvider::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}