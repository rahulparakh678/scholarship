<?php

namespace App\Http\Controllers\Student;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Scholarship;
use Auth;
use App\StudentProfile;
use App\StudStatus;
use DB;
class HomeController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
        $this->middleware(['auth','verified']);

    }
    public function index()
    {

        if(StudentProfile::where('user_id',auth()->user()->id)->exists())
        {
             $profile=StudentProfile::where('user_id',Auth::user()->id)->first();
             return view('students.home',compact('profile'));
        }
        else
        {
            return view('students.home');
        }
        
    }
    public function myscholarship()
    {
        $profile=StudentProfile::where('user_id',Auth::user()->id)->first();
    	$scholarships=Scholarship::where('status','Active')->get();
    	return view('students.viewscholarships',compact('scholarships','profile'));
    }
    public function showdetails($id)
    {
    	$scholarships=Scholarship::find($id);
        $studentcourse=DB::table('course_scholarship')->where('scholarship_id',$id)->get();
    	return view('students.scholarshipdetails',compact('scholarships','studentcourse'));
    }
    public function apply(Request $request,$id)
    {
       $status='Application Status Submitted Successfully';
        $schemeid=Scholarship::find($id);
        $schemeid->users()->attach(Auth::user()->id,['user_name'=>Auth::user()->name,'status' => 'Application Submitted']);
        
        return redirect()->back()->with('message','Successfully Applied to Scholarship');
    }

    public function appliedscholarship()
    {
       // $scholarships=\DB::table('scholarship_user')->where('user_id',auth::user()->id)->get();
        //$scholarships=Scholarship::->users->where('id',auth()->user()->id)->get();
        //$scholarships=$scholarships->users->where('id',auth()->user()->id)->get();
        
        //$scholarships=Scholarship::has('users')->where('user_id',auth()->user()->id)->get();
        //$scholarships=Scholarship::where('user_id',auth()->user()->id)->get();
        //$scholarships=Scholarship::all();
        $scholarships=Scholarship::all();
        $StudStatus=StudStatus::where('user_id',auth()->user()->id)->get();
       // $scholarships=$scholarships->users()->where('id',auth()->user()->id)->get();
        //$scholarships=$scholarships->users->where('id',auth()->user()->id)->get();
        //$scholarships->users()->get();
        //$scholarships=$scholarships->users()->wherePivot('user_id',auth()->user()->id)->get();   
        
        return view('students.appliedscholarship',compact('scholarships','StudStatus'));
    }

    
}
