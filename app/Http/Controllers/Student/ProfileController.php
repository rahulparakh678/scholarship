<?php
namespace App\Http\Controllers\Student;

use App\Caste;
use App\Coursetype;
use App\Course;
use App\StudentProfile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\StoreStudentProfileRequest;
use App\Http\Requests\UpdateStudentProfileRequest;
use Storage;
use auth;
use Redirect;
use App\StudentCourses;

class ProfileController extends Controller
{
    //

    public function getcourse(Request $request)
    {
        if(!$request->course_type_id)
        {
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
        }
        else
        {
            $html = '';
            $course_names = Course::where('course_type_id', $request->course_type_id)->get();
        foreach ($course_names as $course_name) {
            $html .= '<option value="'.$course_name->id.'">'.$course_name->course_name.'</option>';
            }   
        }
        return response()->json(['html' => $html]);
    }
    public function create()

    {
    	
        //
        if(StudentProfile::where('user_id',auth()->user()->id)->exists())
        {
            $profile=StudentProfile::where('user_id',auth()->user()->id)->first();
            $castes = Caste::all()->pluck('caste_name', 'id')->prepend(trans('global.pleaseSelect'), '');
            $course_types = Coursetype::all()->pluck('course_type_name', 'id')->prepend(trans('Please select course type'), '');
            return view('students.profile.create',compact('castes','course_types','profile'));
        }
        

        $castes = Caste::all()->pluck('caste_name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $course_types = Coursetype::all()->pluck('course_type_name', 'id')->prepend(trans('Please select course type'), '');

           //$course_names = Course::all()->pluck('course_name', 'id')->prepend(trans('global.pleaseSelect'), '');
            return view('students.profile.create',compact('castes','course_types'));

    	
    }

    public function preview()
    {
        return view('students.profile.preview');
    }

    public function updatedetails(Request $request)
    {
        
            //$personaldetails=StudentProfile::create($request->all());
            $personaldetails=StudentProfile::where('user_id',auth()->user()->id)->first();
            $personaldetails->update($request->all());
            return redirect()->back()->with('message',' Details Saved Successfully');
        
       
        
    }
    public function servicedetails(Request $request)
    {
        
            //$personaldetails=StudentProfile::create($request->all());
            $personaldetails=StudentProfile::where('user_id',auth()->user()->id)->first();
            $personaldetails->update($request->all());
            
        
          return Redirect::to('/students')->with('message','Your Profile is Completed Successfully. Our Verification team will contact you in next 2-3 days for KYC Process');
        
    }


    public function sfc()
    {
        return view('students.sfc');
    }
    
    

    public function store(StoreStudentProfileRequest $request)
    {
        
    	$studentDetail = StudentProfile::create($request->all());
    	//return redirect()->back()->with('message','Profile is completed successfully Now Upload documents in documents Section');
        //$personaldetails=StudentProfile::create($request->all());
        //return redirect()->back()->with('Personal Details Saved Successfully');
        $castes = Caste::all()->pluck('caste_name', 'id')->prepend(trans('global.pleaseSelect'), '');

         $course_types = Coursetype::all()->pluck('course_type_name', 'id')->prepend(trans('Please select course type'), '');
         $course = Course::all()->pluck('course_name', 'id')->prepend(trans('Please select course'), '');
         $studentcourse=StudentCourses::all()->pluck('course_name','id')->prepend(trans('Please select course'), '');
         $profile=StudentProfile::where('user_id',auth()->user()->id)->first();
        
         return view('students.profile.edit', compact('profile','castes','course_types','course','studentcourse'));


    }

    public function edit(StudentProfile $profile,$id)
    {
        $profile=StudentProfile::where('user_id',$id)->first();
        $castes = Caste::all()->pluck('caste_name', 'id')->prepend(trans('global.pleaseSelect'), '');

         $course_types = Coursetype::all()->pluck('course_type_name', 'id')->prepend(trans('Please select course type'), '');

         $course = Course::all()->pluck('course_name', 'id')->prepend(trans('Please select course'), '');
         $studentcourse=StudentCourses::all()->pluck('course_name', 'id')->prepend(trans('Please select course'), '');
        
        return view('students.profile.edit', compact('profile','castes','course_types','course','studentcourse'));
    }

    public function update(UpdateStudentProfileRequest $request, StudentProfile $profile)
    {
        $profile->update($request->all());

        //return redirect()->back()->with('message','Profile is Updated successfully Now Upload documents in documents Section');

        return Redirect::to('/students')->with('message','Profile Updated successfully');
    }

    public function documents()
    {
        $user_id=auth()->user()->id;
        $profiles=StudentProfile::where('user_id',$user_id)->first();
        return view('students.profile.documents',compact('profiles'));
    }

    public function aadharupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $aadhar_card=$request->file('aadhar_card')->store('documents','s3');
        Storage::disk('s3')->setVisibility($aadhar_card,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'aadhar_card' => Storage::disk('s3')->url($aadhar_card)
        ]);
        return redirect()->back()->with('message','Aadhar card uploaded successfully');
    }
    public function panupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $pan_card=$request->file('pan_card')->store('documents','s3');
        Storage::disk('s3')->setVisibility($pan_card,'public');
        StudentProfile::where('user_id',$user_id)->update([
            'pan_card' => Storage::disk('s3')->url($pan_card)
        ]);
        return redirect()->back()->with('message','Pan card uploaded successfully');
    }
    public function casteupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $caste_certificate=$request->file('caste_certificate')->store('documents','s3');
        Storage::disk('s3')->setVisibility($caste_certificate,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'caste_certificate' => Storage::disk('s3')->url($caste_certificate)
        ]);
        return redirect()->back()->with('message','Caste Certificate uploaded successfully');
    }
    public function phupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $physically_handicapped_certificate=$request->file('physically_handicapped_certificate')->store('documents','s3');
        Storage::disk('s3')->setVisibility($physically_handicapped_certificate,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'physically_handicapped_certificate' => Storage::disk('s3')->url($physically_handicapped_certificate)
        ]);
        return redirect()->back()->with('message','Physically Handicapped Certificate uploaded successfully');
    }
    public function deathupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $death_certificate=$request->file('death_certificate')->store('documents','s3');
        Storage::disk('s3')->setVisibility($death_certificate,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'death_certificate' => Storage::disk('s3')->url($death_certificate)
        ]);
        return redirect()->back()->with('message','Death Certificate uploaded successfully');
    }
     public function photoupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');

        $photo=$request->file('photo')->store('documents','s3');
        Storage::disk('s3')->setVisibility($photo,'public');
        StudentProfile::where('user_id',$user_id)->update([
            'photo' => Storage::disk('s3')->url($photo)
        ]);
        
        
        return redirect()->back()->with('message','Photo uploaded successfully');
    }
     public function addproofupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');

        $address_proof=$request->file('address_proof')->store('documents','s3');
        Storage::disk('s3')->setVisibility($address_proof,'public');
        StudentProfile::where('user_id',$user_id)->update([
            'address_proof' => Storage::disk('s3')->url($address_proof)
        ]);
        
        
        return redirect()->back()->with('message','Address Proof uploaded successfully');
    }
    public function domupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $domicile_certificate=$request->file('domicile_certificate')->store('documents','s3');
        Storage::disk('s3')->setVisibility($domicile_certificate,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'domicile_certificate' => Storage::disk('s3')->url($domicile_certificate)
        ]);
        return redirect()->back()->with('message','Domicile Certificate uploaded successfully');
    }
    public function icupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $income_certificate=$request->file('income_certificate')->store('documents','s3');
        Storage::disk('s3')->setVisibility($income_certificate,'public');
        StudentProfile::where('user_id',$user_id)->update([
            'income_certificate' => Storage::disk('s3')->url($income_certificate)
        ]);
        return redirect()->back()->with('message','Income Certificate uploaded successfully');
    }
    public function passbookupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $bank_passbook=$request->file('bank_passbook')->store('documents','s3');
        Storage::disk('s3')->setVisibility($bank_passbook,'public');
        StudentProfile::where('user_id',$user_id)->update([
            'bank_passbook' => Storage::disk('s3')->url($bank_passbook)
        ]);
        return redirect()->back()->with('message','Bank Passbook uploaded successfully');
    }
    public function clgidupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $clg_id_card=$request->file('clg_id_card')->store('documents','s3');
        Storage::disk('s3')->setVisibility($clg_id_card,'public');
        StudentProfile::where('user_id',$user_id)->update([
            'clg_id_card' => Storage::disk('s3')->url($clg_id_card)
        ]);
        return redirect()->back()->with('message','ID Card uploaded successfully');
    }
    public function bonafideupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $bonafide_certificate=$request->file('bonafide_certificate')->store('documents','s3');
        Storage::disk('s3')->setVisibility($bonafide_certificate,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'bonafide_certificate' => Storage::disk('s3')->url($bonafide_certificate)
        ]);
        return redirect()->back()->with('message','Bonafide Certificate uploaded successfully');
    }
    public function admissionupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $admission_letter=$request->file('admission_letter')->store('documents','s3');
        Storage::disk('s3')->setVisibility($admission_letter,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'admission_letter' => Storage::disk('s3')->url($admission_letter)
        ]);
        return redirect()->back()->with('message','Admission Letter uploaded successfully');
    }
    public function cfupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $currentyear_fees_reciept=$request->file('currentyear_fees_reciept')->store('documents','s3');
        Storage::disk('s3')->setVisibility($currentyear_fees_reciept,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'currentyear_fees_reciept' => Storage::disk('s3')->url($currentyear_fees_reciept)
        ]);
        return redirect()->back()->with('message','Fees Reciept uploaded successfully');
    }
    public function hfupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $hostel_reciept=$request->file('hostel_reciept')->store('documents','s3');
        Storage::disk('s3')->setVisibility($hostel_reciept,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'hostel_reciept' => Storage::disk('s3')->url($hostel_reciept)
        ]);
        return redirect()->back()->with('message','Hostel Fees Reciept uploaded successfully');
    }
    public function class10upload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $class10_marksheet=$request->file('class10_marksheet')->store('documents','s3');
        Storage::disk('s3')->setVisibility($class10_marksheet,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'class10_marksheet' => Storage::disk('s3')->url($class10_marksheet)
        ]);
        return redirect()->back()->with('message','Class 10 Marksheet uploaded successfully');
    }
    public function class12upload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $class12_marksheet=$request->file('class12_marksheet')->store('documents','s3');
        Storage::disk('s3')->setVisibility($class12_marksheet,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'class12_marksheet' => Storage::disk('s3')->url($class12_marksheet)
        ]);
        return redirect()->back()->with('message','Class 12 Marksheet uploaded successfully');
    }
    public function diplomaupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $diploma_marksheet=$request->file('diploma_marksheet')->store('documents','s3');
        Storage::disk('s3')->setVisibility($diploma_marksheet,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'diploma_marksheet' => Storage::disk('s3')->url($diploma_marksheet)
        ]);
        return redirect()->back()->with('message','Diploma Marksheet uploaded successfully');
    }
    public function gradupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $graduation_marksheet=$request->file('graduation_marksheet')->store('documents','s3');
        Storage::disk('s3')->setVisibility($graduation_marksheet,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'graduation_marksheet' => Storage::disk('s3')->url($graduation_marksheet)
        ]);
        return redirect()->back()->with('message','Graduation Marksheet uploaded successfully');
    }

    public function prevupload(Request $request)
    {
        $user_id=auth()->user()->id;
        //$request->aadhar_card->storeAs('logos','aadhar.png');
        $previous_marksheet=$request->file('previous_marksheet')->store('documents','s3');
        Storage::disk('s3')->setVisibility($previous_marksheet,'public');
        
        StudentProfile::where('user_id',$user_id)->update([
            'previous_marksheet' => Storage::disk('s3')->url($previous_marksheet)
        ]);
        return redirect()->back()->with('message','Previous Year/Semester Marksheet uploaded successfully');
    }







    
}
