<?php

namespace App\Http\Controllers\SFCNGO;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\StudentProfile;
use App\StudStatus;
use DB;
use Gate;
use Symfony\Component\HttpFoundation\Response;
class HomeController extends Controller
{
    //
    public function index()
    {
        $results=StudentProfile::where('ref_code',auth()->user()->ref_code)->count();
        //$students=StudStatus::where()
        return view('sfcngo.home',compact('results'));

    }
    public function sfc_student_list()
    {
    	abort_if(Gate::denies('profile_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $results=StudentProfile::where('ref_code',auth()->user()->ref_code)->get();
    	return view('sfcngo.studentlist',compact('results'));
    }
    public function scholarship_applications()
    {
    	$results=StudentProfile::where('ref_code',auth()->user()->ref_code)->get();
    	return view('sfcngo.studentlist',compact('results'));
    }

    public function sfcapplications( )
    {
    	 	
    	 

            //$results=StudStatus::where('user_id',$user_id)->get();
    		//$results=DB::table('stud_statuses')->where('user_id',$Request->user_id)->get();
           
            //return redirect()->back()->with('message','User Found Successfully',compact('studstatus'));
    	    $profiles=StudentProfile::where('ref_code',auth()->user()->ref_code)->get();
            return view('sfcngo.applicationstatus',compact('profiles'));

       
    }

     public function searchstatus(Request $request)
    {
        $user_id=$request->input('user_id');
        if (StudStatus::where('user_id',$user_id)->exists()) {

            $studstatus=StudStatus::where('user_id',$user_id)->get();
           
            //return redirect()->back()->with('message','User Found Successfully',compact('studstatus'));
            //return view('admin.profiles.editstatus',compact('studstatus'));
            return view('sfcngo.scholarshiplist',compact('studstatus'));

        }
        else
        {
            
            return redirect()->back()->with('message','User Not  Found ',compact('user_id'));
        }
        
    }

     public function profileshow(StudentProfile $profile,$id)
    {
       abort_if(Gate::denies('profile_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $profile=StudentProfile::where('user_id',$id)->first();
        return view('sfcngo.viewprofile', compact('profile'));
    }
}
