<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Scholarship;
use App\ScholarshipAchiever;
use Redirect;
use App\Fraud;
use Excel;
use App\FaqQuestion;
use App\Coursetype;
use App\Course;
use DB;
class HompageController extends Controller
{
    //
    
    public function index()
    {	
    	//$scholarships=Scholarship::latest()->limit(5)->where('status','Active')->get();
    	return view('welcome');
    }

    public function ulf()
    {
        return view('auth.ngo_register');
    }

    public function aboutus()
    {
        return view('partials.template.aboutus');
    }

    public function educo()
    {
        return view('auth.educo_register');
    }

    public function rsml()
    {
        return view('auth.rsml');
    }

    public function amp()
    {
        return view('auth.amp_register');
    }

    public function executive()
    {
        return view('auth.exec_register');
    }
    public function sparsha()
    {
        return view('auth.sparsha');
    }
    public function services()
    {
        return view('partials.template.services');
    }


    public function scholarships(Request $request)
    {
        $category=request('category');
        if ($category) {
            # code...
            $scholarships=Scholarship::where('category_id',$category)->paginate(5);
            return view('students.als',compact('scholarships'));
        }else{
            $scholarships=Scholarship::latest()->paginate(6);
            return view('students.als',compact('scholarships'));   
        }
    	
    }

    public function showsdetails($id)
    {
        $scholarships=Scholarship::find($id);
        $studentcourse=DB::table('course_scholarship')->where('scholarship_id',$id)->get();
        return view('partials.template.sdetails',compact('scholarships','studentcourse'));
    }

    public function achievers()
    {
        $achievers=ScholarshipAchiever::orderBy('scholarshipamount','DESC')->paginate(6);
        return view('partials.template.achiever',compact('achievers'));
    }

    public function terms()
    {
        return view('partials.template.terms_conditions');
    }

    public function privacy()
    {
        return view('partials.template.privacy');
    }

    public function disclaimer()
    {
        return view('partials.template.disclaimer');
    }

    public function upload(Request $request)
    {
         $request->logo->storeAs('logos','logo1.png');
         return Redirect::back();
    }

    public function faq()
    {
        $faqs=FaqQuestion::all();
        return view('partials.template.faq',compact('faqs'));
    }

    public function activescholarships()
    {
        $course_types = Coursetype::all()->pluck('course_type_name', 'id')->prepend(trans('Please select course type'), '');
        $scholarships=Scholarship::orderBy('status','ASC')->paginate(9);
        return view('students.activescholarship',compact('scholarships','course_types'));
    }
    public function getcourses(Request $request)
    {
        if(!$request->course_type_id)
        {
            $html = '<option value="">'.trans('global.pleaseSelect').'</option>';
        }
        else
        {
            $html = '';
            $course_names = Course::where('course_type_id', $request->course_type_id)->get();
        foreach ($course_names as $course_name) {
            $html .= '<option value="'.$course_name->id.'">'.$course_name->course_name.'</option>';
            }   
        }
        return response()->json(['html' => $html]);
    }
}
