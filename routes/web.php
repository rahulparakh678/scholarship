<?php

Route::view('demo','demo');
Route::get('/aboutus','HompageController@aboutus');
Route::get('/ulf','HompageController@ulf');
Route::get('/educo','HompageController@educo');
Route::get('/rsml','HompageController@rsml');
Route::get('/amp','HompageController@amp');
Route::get('/exec','HompageController@executive');
Route::get('/sparsha','HompageController@sparsha');

Route::get('allstatus','Admin\ProfilesController@allstatus')->name('allstatus');

 Route::get('studstatus','Admin\ProfilesController@scholarshipstatus')->name('studstatus');
 Route::get('/updatescholarshipstatus/{id}','Admin\ProfilesController@updatescholarshipstatus')->name('updatescholarshipstatus');
 Route::get('/editstudstatus','Admin\ProfilesController@editscholarshipstatus')->name('editstudstatus');
Route::get('/editstudentprofile/{id}','Admin\ProfilesController@edit')->name('editstudentprofile');
 Route::POST('storestatus','Admin\ProfilesController@storestatus')->name('storestatus');

Route::POST('updatestatus/{id}','Admin\ProfilesController@updatestatus')->name('updatestatus');
Route::POST('searchstatus','Admin\ProfilesController@searchstatus')->name('searchstatus');

Route::get('/sdetails/{id}','HompageController@showsdetails')->name('scholarshipdetails'); 
//Route::POST('/upload','HompageController@upload')->name('upload');
Route::get('/achievers','HompageController@achievers')->name('achievers');
Route::get('/services','HompageController@services')->name('services');
Route::POST('/enquiry','Admin\EnquiryController@create')->name('enquiry');
Route::get('enquiryform','Admin\EnquiryController@index')->name('enquiryform');
Route::delete('deleteenquiry/{id}','Admin\EnquiryController@destroy')->name('deleteenquiry');
Route::get('/terms','HompageController@terms')->name('terms');
Route::get('/privacy','HompageController@privacy')->name('privacy');
Route::get('/disclaimer','HompageController@disclaimer')->name('disclaimer');
Route::get('active','HompageController@activescholarships')->name('activescholarships');
Route::get('course/get_coursetype','HompageController@getcourses')->name('getcourses');
Route::get('/faq','HompageController@faq')->name('faq');
Route::get('/','HompageController@index');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

//Auth::routes(['register' => true]);
// Admin
Auth::routes(['verify' => true]);

Route::GET('filterview','Admin\ProfilesController@filterview')->name('filterview');
Route::GET('edit_course/{id}','Admin\StudentCourseController@edit')->name('edit_course');
Route::GET('/studentcourses','Admin\StudentCourseController@index')->name('student_courses');
Route::GET('/student_course_create','Admin\StudentCourseController@create')->name('student_course_create');
Route::POST('student_course_store','Admin\StudentCourseController@store')->name('student_course_store');
Route::POST('student_course_update/{id}','Admin\StudentCourseController@update')->name('student_course_update');



Route::POST('paid/{id}','Admin\ProfilesController@paid')->name('paid');
Route::POST('sfcstu/{id}','Admin\ProfilesController@sfcstu')->name('sfcstu');
Route::get('scholarship-providers/sfc-ngo-create', 'Admin\ScholarshipProvidersController@sfccreate')->name('sfccreate');
Route::POST('scholarship-providers/sfc-ngo', 'Admin\ScholarshipProvidersController@sfcstore')->name('sfcstore');
Route::POST('/updatestudentdetails/{id}','Admin\ProfilesController@updatedetails')->name('updatestudentdetails');
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Castes
    Route::delete('castes/destroy', 'CasteController@massDestroy')->name('castes.massDestroy');
    Route::resource('castes', 'CasteController');

    // Coursetypes
    Route::delete('coursetypes/destroy', 'CoursetypeController@massDestroy')->name('coursetypes.massDestroy');
    Route::resource('coursetypes', 'CoursetypeController');

    // Courses
    Route::delete('courses/destroy', 'CoursesController@massDestroy')->name('courses.massDestroy');
    Route::resource('courses', 'CoursesController');

    // Categories
    Route::delete('categories/destroy', 'CategoryController@massDestroy')->name('categories.massDestroy');
    Route::resource('categories', 'CategoryController');

    // Scholarship Providers

   
    Route::delete('scholarship-providers/destroy', 'ScholarshipProvidersController@massDestroy')->name('scholarship-providers.massDestroy');
    Route::resource('scholarship-providers', 'ScholarshipProvidersController');

    // Scholarships

    Route::delete('scholarships/destroy', 'ScholarshipsController@massDestroy')->name('scholarships.massDestroy');
    Route::post('scholarships/media', 'ScholarshipsController@storeMedia')->name('scholarships.storeMedia');
    Route::post('scholarships/ckmedia', 'ScholarshipsController@storeCKEditorImages')->name('scholarships.storeCKEditorImages');
    Route::resource('scholarships', 'ScholarshipsController');

   

    // Profiles
    Route::delete('profiles/destroy', 'ProfilesController@massDestroy')->name('profiles.massDestroy');
    Route::resource('profiles', 'ProfilesController');



    // Scholarship Achievers
    Route::delete('scholarship-achievers/destroy', 'ScholarshipAchieversController@massDestroy')->name('scholarship-achievers.massDestroy');
    Route::post('scholarship-achievers/media', 'ScholarshipAchieversController@storeMedia')->name('scholarship-achievers.storeMedia');
    Route::post('scholarship-achievers/ckmedia', 'ScholarshipAchieversController@storeCKEditorImages')->name('scholarship-achievers.storeCKEditorImages');
    Route::resource('scholarship-achievers', 'ScholarshipAchieversController');

     // Ticketcategories
    Route::delete('ticketcategories/destroy', 'TicketcategoriesController@massDestroy')->name('ticketcategories.massDestroy');
    Route::resource('ticketcategories', 'TicketcategoriesController');

    // Tickets
    Route::delete('tickets/destroy', 'TicketsController@massDestroy')->name('tickets.massDestroy');
    Route::resource('tickets', 'TicketsController');

    // Faq Categories
    Route::delete('faq-categories/destroy', 'FaqCategoryController@massDestroy')->name('faq-categories.massDestroy');
    Route::resource('faq-categories', 'FaqCategoryController');

    // Faq Questions
    Route::delete('faq-questions/destroy', 'FaqQuestionController@massDestroy')->name('faq-questions.massDestroy');
    Route::resource('faq-questions', 'FaqQuestionController');
    
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
    }
});



Route::get('/students','Student\HomeController@index')->name('students.home');

Route::get('/preview','Student\ProfileController@preview')->name('preview');
//Student Profile Controller
Route::group(['prefix' => 'student','namespace' => 'Student'], function(){

Route::get('/createprofile','ProfileController@create')->name('createprofile');

Route::POST('/details','ProfileController@updatedetails')->name('details');
Route::POST('/servicepreview','ProfileController@servicedetails')->name('servicepreview');

Route::POST('/createprofile','ProfileController@store')->name('storeprofile');
Route::get('/editprofile/{id}','ProfileController@edit')->name('editprofile');
Route::POST('updateprofile/{id}','ProfileController@update')->name('updateprofile');



Route::get('course/get_coursetype','ProfileController@getcourse')->name('getcourse');

Route::get('documents','ProfileController@documents')->name('documents');
Route::POST('aadharupload','ProfileController@aadharupload')->name('aadharupload');

Route::POST('panupload','ProfileController@panupload')->name('panupload');
Route::POST('casteupload','ProfileController@casteupload')->name('casteupload');
Route::POST('phupload','ProfileController@phupload')->name('phupload');
Route::POST('deathupload','ProfileController@deathupload')->name('deathupload');
Route::POST('photoupload','ProfileController@photoupload')->name('photoupload');
Route::POST('addproofupload','ProfileController@addproofupload')->name('addproofupload');
Route::POST('domupload','ProfileController@domupload')->name('domupload');
Route::POST('icupload','ProfileController@icupload')->name('icupload');
Route::POST('passbookupload','ProfileController@passbookupload')->name('passbookupload');
Route::POST('clgidupload','ProfileController@clgidupload')->name('clgidupload');
Route::POST('bonafideupload','ProfileController@bonafideupload')->name('bonafideupload');
Route::POST('admissionupload','ProfileController@admissionupload')->name('admissionupload');
Route::POST('cfupload','ProfileController@cfupload')->name('cfupload');
Route::POST('hfupload','ProfileController@hfupload')->name('hfupload');
Route::POST('class10upload','ProfileController@class10upload')->name('class10upload');
Route::POST('class12upload','ProfileController@class12upload')->name('class12upload');
Route::POST('diplomaupload','ProfileController@diplomaupload')->name('diplomaupload');
Route::POST('gradupload','ProfileController@gradupload')->name('gradupload');
Route::POST('prevupload','ProfileController@prevupload')->name('prevupload');



//sfc

Route::get('preview','ProfileController@sfc')->name('sfc');



 //Student Search & Apply Scholarship

Route::get('/myscholarship','HomeController@myscholarship')->name('myscholarship');

Route::get('/details/{id}','HomeController@showdetails')->name('showdetails');

//Support Controller
Route::get('/support','TicketsController@index')->name('support');

Route::get('/createquery','TicketsController@create')->name('createquery');
Route::POST('storequery','TicketsController@store')->name('storequery');

Route::get('viewresponse/{id}','TicketsController@show')->name('response');
});

 Route::get('showapplications/{id}','SetupscholarshipController@showapplications')->name('showapplications');

Route::get('filteredview/{id}','ScholarshipProvider\SetupscholarshipController@filteredview')->name('filteredview');
Route::get('/scholarshipproviders','ScholarshipProvider\HomeController@index')->name('providers.home');
Route::get('/sfcngostudentlist','SFCNGO\HomeController@sfc_student_list')->name('sfc_student_list');

Route::get('/sfcngo','SFCNGO\HomeController@index')->name('sfc.home');

Route::GET('/sfcngo/applications','SFCNGO\HomeController@sfcapplications')->name('sfcapplications');
Route::POST('sfcsearchstatus','SFCNGO\HomeController@searchstatus')->name('sfcsearchstatus');
Route::GET('profileshow/{id}','SFCNGO\HomeController@profileshow')->name('profileshow');

Route::group(['prefix'=>'provider','namespace'=>'ScholarshipProvider'],function(){

     Route::delete('setupscholarships/destroy', 'SetupscholarshipController@massDestroy')->name('massDestroy');
    Route::post('setupscholarships/media', 'SetupscholarshipController@storeMedia')->name('setupscholarships.storeMedia');
    Route::post('setupscholarships/ckmedia', 'SetupscholarshipController@storeCKEditorImages')->name('setupscholarships.storeCKEditorImages');
    Route::resource('setupscholarships', 'SetupscholarshipController');

    Route::get('promotion','SetupscholarshipController@promotion')->name('promotion');
    Route::get('setup','SetupscholarshipController@index')->name('setup');
    Route::get('listscheme','SetupscholarshipController@listscheme')->name('listscheme');
     Route::get('createschol','SetupscholarshipController@create')->name('createscholarship');
       Route::POST('createschol','SetupscholarshipController@store')->name('storescholarship');

    Route::get('show/{id}','SetupscholarshipController@show')->name('showscholarship');
    Route::get('edit/{id}','SetupscholarshipController@edit')->name('editscholarship');
    Route::POST('edit/{id}','SetupscholarshipController@update')->name('updatescholarship');
    Route::DELETE('delete/{id}','SetupscholarshipController@destroy')->name('deletescholarship');

    Route::get('showp/{id}','SetupscholarshipController@showprofile')->name('showprofile');
    Route::get('showapplications/{id}','SetupscholarshipController@showapplications')->name('showapplications');



});
Route::POST('applications/{id}','Student\HomeController@apply')->name('apply');
Route::get('/shortlisted/{id}','ScholarshipProvider\HomeController@Shortlised')->name('Shortlised');
Route::get('/rejected/{id}','ScholarshipProvider\HomeController@Rejected')->name('Rejected');
Route::get('/awarded/{id}','ScholarshipProvider\HomeController@Awarded')->name('Awarded');
Route::get('scholarships/applications','Admin\ScholarshipsController@applicant')->name('viewapplicant');
Route::get('/sco/{id}','Admin\ScholarshipsController@sco')->name('sco');
Route::get('appliedscholarship','Student\HomeController@appliedscholarship')->name('appliedscholarship');

Route::get('/allscholarship','HompageController@scholarships')->name('allscholarship');