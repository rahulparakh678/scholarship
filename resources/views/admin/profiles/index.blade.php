@extends('layouts.admin')
@section('content')
@can('profile_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.profiles.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.profile.title_singular') }}
            </a>
            <a class="btn btn-success" href="{{route('studstatus')}}">
                Add Scholarship Status
            </a>
            <a class="btn btn-success" href="{{route('editstudstatus')}}">
                Edit Scholarship Status
            </a>
            <a class="btn btn-success" href="{{route('allstatus')}}">
                View All Status
            </a>
             <a class="btn btn-success" href="{{route('filterview')}}">
                Filtered View
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.profile.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">

        <div class="table-responsive">

            <table class="table table-bordered table-striped table-hover datatable datatable-Profile">

              
                <thead>
                    <tr>

                  
                        <th width="10">

                        </th>
                        <th >
                            Profile {{ trans('cruds.profile.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.full_name') }}
                        </th>
                        <th>
                            Payment Status
                        </th>
                        <th width="10%">
                          Mobile Number
                        </th>
                        <th>
                            {{ trans('cruds.profile.fields.email') }}
                        </th>
                        
                        
                        
                        
                        
                        
                        
                        <th>
                            Permanent State
                        </th>
                        
                        <th>
                            Course Type
                        </th>
                        
                        <th>
                            Course Specialization
                        </th>
                        
                        
                        
                        
                        <th>
                            Referral Code
                        </th>
                        
                    </tr>
                    <tr>
                      <td>
                        
                      </td>
                      <td>
                           <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                      </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            
                        </td>
                        <td>
                           <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                      </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        
                        
                        
                        
                        
                        
                        
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        
                        <td>
                            
                            <select class="search">
                            <option >{{ trans('global.all') }}</option
                              >
                              @foreach(App\Coursetype::all() as $label)
                                <option value="{{$label->course_type_name}}">{{$label->course_type_name}}</option>
                              @endforeach
                            
                          </select>
                        </td>
                        
                        <td>
                             <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        
                        
                        
                        
                        <td>
                             <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                       
                      
                    </tr>
                    
                </thead>
                <tbody>
                    @foreach($profiles as $chunk)
                    @foreach($chunk as $key => $profile)
                        <tr >
                            <td>
                                    <br>
                                    @can('profile_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.profiles.show', $profile->id) }}" target="_blank">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('profile_delete')
                                    <form action="{{ route('admin.profiles.destroy', $profile->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan                            
                                
                                
                            </td>
                            <td>
                                {{ $profile->id }}

                                
                                
                                
                            </td>
                            <td>
                                {{ $profile->fullname ?? '' }}
                                


                               


                            </td>
                            <td>
                                @if($profile->paid === 'YES')
                                 <span class="badge badge-success">Paid</span>

                                @elseif($profile->paid === 'SFC')
                                 <span class="badge badge-warning">SFC Student</span>

                                @else
                                 <span class="badge badge-danger">Not Paid</span>
                                 @endif
                            </td>
                            <td>
                                {{$profile->mobile}}
                            </td>
                            <td>
                                {{ $profile->email ?? '' }}
                            </td>
                            
                                                                            
                            <td>
                                {{ $profile->permanent_state ?? '' }}
                            </td>
                            
                            <td>
                                {{ $profile->course_type->course_type_name ?? '' }}
                                
                            </td>
                            
                            <td>
                                {{$profile->student_course_name->course_name ?? ''}}
                                
                            </td>
                           
                            
                            
                            
                            
                                
                            
                            <td>
                                
                                   {{$profile->ref_code}}
                            </td>

                        </tr>
                    @endforeach
                    @endforeach
                </tbody>
            </table>

            <?php /*
            {{ -- $profile->appends(Illuminate\Support\Facades\Request::except('page'))--}} */?>
        </div>
    </div>
 
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {

         let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
$.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'DESC' ]],
    pageLength: 10,
  });
  

 table = $('.datatable-Profile:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  $('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value
      table
        .column($(this).parent().index())
        .search(value, strict)
        .draw()
  });
  
})

</script>
@endsection