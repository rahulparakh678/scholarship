@extends('layouts.admin')
@section('content')
@can('scholarship_provider_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.scholarship-providers.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.scholarshipProvider.title_singular') }}
            </a>

            <a class="btn btn-success" href="{{route('sfccreate')}}">
                ADD SFC NGO
            </a>
        </div>
    </div>
@endcan
<div class="card">
    <div class="card-header">
        {{ trans('cruds.scholarshipProvider.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-ScholarshipProvider">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.scholarshipProvider.fields.id') }}
                        </th>
                        <th>
                            {{ trans('cruds.scholarshipProvider.fields.organization_name') }}
                        </th>
                        <th>
                            {{ trans('cruds.scholarshipProvider.fields.contact_person') }}
                        </th>
                        <th>
                            {{ trans('cruds.scholarshipProvider.fields.designation') }}
                        </th>
                        <th>
                            {{ trans('cruds.scholarshipProvider.fields.email') }}
                        </th>
                        <th>
                            Action&nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($scholarshipProviders as $key => $scholarshipProvider)
                        <tr data-entry-id="{{ $scholarshipProvider->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $scholarshipProvider->id ?? '' }}
                            </td>
                            <td>
                                {{ $scholarshipProvider->organization_name ?? '' }}
                            </td>
                            <td>
                                {{ $scholarshipProvider->contact_person ?? '' }}
                            </td>
                            <td>
                                {{ $scholarshipProvider->designation ?? '' }}
                            </td>
                            <td>
                                {{ $scholarshipProvider->email ?? '' }}
                            </td>
                            <td>
                                @can('scholarship_provider_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.scholarship-providers.show', $scholarshipProvider->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('scholarship_provider_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.scholarship-providers.edit', $scholarshipProvider->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('scholarship_provider_delete')
                                    <form action="{{ route('admin.scholarship-providers.destroy', $scholarshipProvider->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('scholarship_provider_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.scholarship-providers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-ScholarshipProvider:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection