<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    
    <meta name="keywords" content="FORSTU,Scholarships,  creative, html">
    <meta name="robots" content="index,follow"/>
    <meta name="googlebot" content="index,follow"/>
    <meta name="description" content="FOrSTU is India&#x27;s largest scholarship platform that connects scholarship and education loan providers with seekers. Find online scholarships and apply now."/>
    <meta property="og:url" content="https://www.forstu.co"/>
    <meta property="og:title" content="Scholarship portal for Indian students | Find online scholarships info"/>
    <meta property="og:description" content="FORSTU is India&#x27;s largest scholarship platform that connects scholarship and education loan providers with seekers. Find online scholarships and apply now."/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>


    
       <!-- Font Awesome -->
<script src="https://use.fontawesome.com/0b0c8d0220.js"></script>

 

    <title>FORSTU |Scholarships | Students</title>
	@include('../partials.template.head')
	
	
</head>
<body>
	
    
    
   
   
    
	@include('../partials.template.nav')
		@yield('content')
		
	@yield('scripts')
	<br>
	@include('../partials.template.footer')
</body>
</html>