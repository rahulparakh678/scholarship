<!DOCTYPE html>
<html lang="en">

<head>
    <title>FORSTU</title>
    @include('partials.template.head')
        <!-- Font Awesome -->
<script src="https://use.fontawesome.com/0b0c8d0220.js"></script>

    <!-- Google Tag Manager -->

</head>

<body>

    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5FSZ96"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
   
   <!-- Header Section Begin -->
  
    @include('partials.template.nav')
    @include('partials.template.hero')
    
    <!-- Home About Begin -->
    <section class="home-about spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="home__about__text">
                        <div class="section-title">
                            <center><h2>SCHOLARSHIP PLATFORM FOR STUDENTS</h2></center>
                            <center><h4>Finance your Education With more than 600 Scholarships listed on this platform.</h4></center>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="home__about__item" style="border-left: 5px solid #88C417 ; padding-left: 10px;">
                                    <h4 >Our Mission</h4>
                                    <p>To make education in India Affordable so that number of dropouts could be reduced and education would be accessible to student at last mile.</p>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="home__about__item" style="border-left: 5px solid #88C417 ; padding-left: 10px;">
                                    <h4>Our Vision</h4>
                                    <p>To help students finance their own education on their own with different ways like Scholarships,Fellowships,Internships.</p>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('register')}}" class="primary-btn">Register Now</a>
                    </div>
                </div>
                <div class="col-lg-5 offset-lg-1">
                    <div class="home__about__img">
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Home About End -->
    @include('partials.template.ourwork')

    <!-- Counter Begin -->
    
    @include('partials.template.impact')

    <!-- Counter End -->
{{--@include('partials.template.category')--}}
    
   
    @include('partials.template.testimonial')
    @include('partials.template.footer')

   <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N5FSZ96');</script>
<!-- End Google Tag Manager --> 

</body>

</html>