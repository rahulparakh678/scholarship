@extends('layouts.student')

@section('content')

<form method="POST" action="{{ route('servicepreview')}}" enctype="multipart/form-data">
@csrf

<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4">Services</h1>
  <p class="lead">Tick on the services you wish to select.Our team will work hard to provide you the best services</p>
</div>
<div class="container">
  <div class="card-deck mb-3 text-center">
    
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal">Register Once Apply Everywhere</h4>
        <span class="lead"><span class="badge badge-pill badge-success ">Improve your Chances of Getting Scholarship by 3x</span></span>

      </div>
      <div class="card-body">
        <h1 class="card-title pricing-card-title">Rs 1500 <small class="text-muted"></small></h1>
        <ul class="list-unstyled mt-3 mb-4">

          <li>
          <i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Your application will be forwarded to every eligible scholarship<br>
         </li>
          <li>
          	<i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Recieve Application PDF & Screenshots of your application <br>
         </li>
          
                    
          <li>
          	<i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Get Applied for All Eligible Scholarships till your  education ends <br>
          </li>
        </ul>
        
      </div>
      <div class="card-footer">
      	Do you wish to avail this service &nbsp;
      	 <input type="radio" name="paid_interest" value="YES" placeholder="YES" required>YES
      	 <input type="radio" name="paid_interest" value="YES" placeholder="YES" required>NO	
      </div>
    </div>
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal">Educational Loan</h4>
      </div>
      <div class="card-body">
        
        <ul class="list-unstyled mt-3 mb-4">
          <li><i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Recieve educational loans from banks</li>
          <li><i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Recieve educational loan from Private Companies</li>
          
          <li><i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Loan for overseas study</li>
        </ul>
        
      </div>
      <div class="card-footer">
      	Do you wish to avail this service &nbsp;
      	 <input type="radio" name="loan_interest" value="YES" placeholder="YES" required>YES
      	 <input type="radio" name="loan_interest" value="NO" placeholder="YES" required>NO	
      </div>
    </div>
  </div>

  <button type="submit" class="btn btn-primary btn-block" style="margin-bottom: 20px;">Submit Profile</button>
</div>
</form>
@endsection