@extends('layouts.student')
@section('content')


<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
  <h1 class="display-4">Services</h1>
  <p class="lead">Tick on the services you wish to select.Our team will work hard to provide you the best services</p>
</div>
<div class="container">
  <div class="card-deck mb-3 text-center">
    
    <div class="card mb-4 shadow-sm">
      <div class="card-header">
        <h4 class="my-0 font-weight-normal">Register Once Apply Everywhere</h4>
        <span class="lead"><span class="badge badge-pill badge-success ">Improve your Chances of Getting Scholarship by 3x</span></span>

      </div>
      <div class="card-body">
        <h1 class="card-title pricing-card-title">Rs 200 <small class="text-muted"></small></h1>
        <ul class="list-unstyled mt-3 mb-4">

          <li>
          <i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Your application will be forwarded to every eligible scholarship<br>
         </li>
          <li>
          	<i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Recieve Application PDF & Screenshots of your application <br>
         </li>
          
                    
          <li>
          	<i class="fa fa-check" aria-hidden="true"></i> &nbsp;
          Get Applied for All Eligible Scholarships till your  education ends <br>
          </li>
        </ul>
        
      </div>
    <form><script src="https://checkout.razorpay.com/v1/payment-button.js" data-payment_button_id="pl_GS161DsXZKUDXy" async> </script> </form>
    
     


    </div>
   

  
</div>

@endsection