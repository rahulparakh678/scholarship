@extends('layouts.student')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    Dashboard
                </div>

                <div class="card-body">
                    @if(session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! {{Auth::user()->name}}

                    <br>
                    <strong>Fullname: &nbsp;</strong>{{Auth::user()->name}}
                    <br>
                    <strong>Email:</strong> &nbsp;{{Auth::user()->email}}
                   

                    <br>
                    <strong>Referral Code:</strong>&nbsp;FS00{{Auth::user()->id}}
                    
                    <br>
                    <a href="{{ route('createprofile') }}" class="btn btn-primary"> Click Here to Complete Profile</a>
                    
                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
@parent

@endsection