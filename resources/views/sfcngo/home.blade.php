@extends('layouts.sfcngo')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    Dashboard
                </div>

                <div class="card-body">
                    @if(session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in! {{Auth::user()->name}}


                    
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-2">
            <div class="alert alert-primary" role="alert">
                <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  Total Students</a> 
                    {{$results}}
            </div>
        </div>
        <div class="col-md-2">
            <div class="alert alert-primary" role="alert">
                <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  Profiles Completed</a> 
            </div>
        </div>
        <div class="col-md-2">
            <div class="alert alert-primary" role="alert">
                <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  KYC Completed</a> 
            </div>
        </div>
        <div class="col-md-2">
            <div class="alert alert-primary" role="alert">
                <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  Total Applications Processed</a> 
            </div>
        </div>
        <div class="col-md-2">
            <div class="alert alert-primary" role="alert">
                <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  Total Awarded</a> 
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection