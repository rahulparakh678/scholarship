@extends('layouts.sfcngo')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('cruds.profile.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">

        <div class="table-responsive">

            <table class="table table-bordered table-striped table-hover datatable datatable-Profile">

              
                <thead>
                    <tr>

                  
                        <th width="10">

                        </th>
                        <th width="10">
                            User ID
                        </th>
                        <th>Scheme Name</th>
                        <th>Application PDF</th>
                        <th>Status</th>
                        
                        
                    </tr>
                   
                    
                </thead>
                <tbody>
                    @foreach($studstatus as $status)

                    <tr>
                        <td></td>
                        <td>{{$status->user_id}}</td>
                        <td>{{$status->scheme_name}}
                            <br>
                            <small>Application Date : {{$status->created_at ->format('j F Y')}}</small>
                        </td>
                        <td width="5">
                            @if(isset($status->applicationpdf))
                                  <a href="{{$status->applicationpdf}}"  target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                            @endif
                        </td>
                        <td> <h4><span class="badge badge-dark">{{$status->status}}</span></h4>   </td>
                    </tr>
                    @endforeach()
                      
                   
                </tbody>
            </table>

            <?php /*
            {{ -- $profile->appends(Illuminate\Support\Facades\Request::except('page'))--}} */?>
        </div>
    </div>
    
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
$.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'DESC' ]],
    pageLength: 10,
  });

 table = $('.datatable-Profile:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  $('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value
      table
        .column($(this).parent().index())
        .search(value, strict)
        .draw()
  });
  
})

</script>
@endsection


