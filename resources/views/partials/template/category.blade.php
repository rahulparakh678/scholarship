<section class="loan-services spad">

     <div class="container">
            <div class="row">
                <div class="col-lg-12">
                <div class="section-title" style="text-align: center;">
                    <h2 style="text-align: center;">Recent Scholarship</h2>
                </div>
            </div>
                @foreach($scholarships as $scholarship)
                <div class="col-md-12">
                    <div class="card border-success mb-3" style="style="max-width: 18rem;>
                        
                        <div class="card-body">
                            <h4>{{$scholarship->scheme_name}}</h4><br>
                            <div class="row">
                                <div class="col-md-5">
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>&nbsp;
            
                                    @foreach($scholarship->courses as $key => $courses)
                    <span class="label label-info"> {{ $courses->course_name }}</span>
                  @endforeach
                                </div>
                                <div class="col-md-5">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp;
                                    {{$scholarship->last_date}}
                                </div>

                                <div class="col-md-2">
                                    <a href="{{ route('showdetails',$scholarship->id)}}">
                                <center>
                                  <button class="btn btn-success ">View Details</button>
                                
                                </center>
                                </a>
                                </div>
                                
                            </div>
                        </div>
                    
                    </div><br>
                    
            </div>
            @endforeach
                
                
        </div>
        </div>
    
</section>
       