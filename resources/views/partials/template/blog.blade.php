<!-- Blog Section Begin -->
    <section class="latest spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Latest Post</h2>
                        <p>Certainly at that point I was not a potential client for the Strib, but promotional</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="latest__blog__item">
                        <h5><a href="#">Get Best Advertiser In Your Side</a></h5>
                        <p>The StarTribune doesn’t just hand out the responsibility of informing their community’s
                            citizenry on a daily basis to just any...</p>
                        <div class="latest__blog__author">
                            <div class="latest__blog__author__pic">
                                <img src="{{asset('external/img/latest/lb-1.png')}}" alt="">
                            </div>
                            <div class="latest__blog__author__text">
                                <h6>May Cain</h6>
                                <span>19th March, 2019</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="latest__blog__item">
                        <h5><a href="#">Internet Advertising Trends You</a></h5>
                        <p>Nope, I had to interview for this job. And I tell you, I was magnificent, so much so that, as
                            you know, I got the position...</p>
                        <div class="latest__blog__author">
                            <div class="latest__blog__author__pic">
                                <img src="{{asset('external/img/latest/lb-2.png')}}" alt="">
                            </div>
                            <div class="latest__blog__author__text">
                                <h6>May Cain</h6>
                                <span>19th March, 2019</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="latest__blog__item">
                        <h5><a href="#">Improve Your Business Cards</a></h5>
                        <p>Upon completion of the interview the gentleman that was to give me my first opportunity at
                            financial freedom...</p>
                        <div class="latest__blog__author">
                            <div class="latest__blog__author__pic">
                                <img src="{{asset('external/img/latest/lb-3.png')}}" alt="">
                            </div>
                            <div class="latest__blog__author__text">
                                <h6>May Cain</h6>
                                <span>19th March, 2019</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 text-center">
                    <a href="#" class="primary-btn">View More</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Blog Section End -->
