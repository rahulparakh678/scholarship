<!-- Counter Begin -->
    
    <div class="counter spad">
        <div class="container">
            <div class="section-title">
                <h2>Our Impact</h2>
                        
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="counter__item">
                        <img src="{{asset(
                        'external/img/counter/counter-4.png')}}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">100</h2><span>+</span><br><br>
                            <h3>Scholarships Given</h3>
                            
                        </div>
                        <p></p>
                    </div>
                </div>
                
                
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="counter__item">
                        <img src="{{asset('external/img/counter/counter-3.png')}}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">{{App\User::count()}}</h2>
                            <span>+</span>
                            <h3>Registered Student</h3>
                            
                        </div>
                        <p></p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6">
                    <div class="counter__item">
                        <img src="{{asset('external/img/counter/counter-1.png')}}" alt="">
                        <div class="counter__number">
                            <h2 class="counter-add">2</h2>
                            <span>Million+</span>
                            <h3>Fund Size Helped</h3>
                        </div>
                        <p></p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
<!-- Counter End -->