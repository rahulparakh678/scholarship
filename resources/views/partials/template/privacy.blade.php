@extends('layouts.main')
@section('content')

<div class="jumbotron jumbotron-fluid">
  <div class="container">
    <h1 class="display-4">Privacy Policy</h1>
    
  </div>
</div>

<div class="container">
	<h3>FORSTU privacy policy for all registered users:</h3>
<p>
At FORSTU, we recognize that privacy is important. FORSTU does not rent, sell, or share personal information about you with other people or nonaffiliated companies except to provide products or services you've requested when we have your permission.
</p>

<p>
A Collection of Information and it's use:

In order to provide our services, we may collect the following types of information:

Personal information (such as your name, email address, phone number and other details) for certain services that we provide through our website.
</p>

<h3>FORSTU use of cookies</h3>
<p>
When you visit FORSTU, we may send one or more cookies - a small file containing a string of characters - to your computer that uniquely identifies your browser. We may use these cookies to improve the quality of our service by storing user preferences and tracking user trends, such as how people search. Most browsers are initially set up to accept cookies, but you can reset your browser to refuse all cookies or to indicate when a cookie is being sent.

Logging information

When you use FORSTU's services, our servers may automatically record information that your browser sends whenever you visit our website. These server logs may include information such as your web request, Internet Protocol address, browser type, browser language, the date and time of your request and one or more cookies that may uniquely identify your browser.

Our communications

When you send an email or other communication to FORSTU, we may retain those communications in order to process your inquiries, respond to your requests and improve our services.
</p>
<h3>Affiliated & Partner Sites</h3>

<p>
We may have affiliated websites who may provide similar or other kinds of services. Personal information that you provide to those sites may be sent to FORSTU and vice versa. We process such information in accordance with this Policy. The affiliated sites may have different privacy practices and we encourage you to read their privacy policies.
</p>
<h3>
Other Website Links</h3>
<p>
FORSTU may present links in a format that enables us to keep track of whether these links have been followed. FORSTU only processes personal information for the purposes described in the applicable Privacy Policy and/or privacy notice for specific services. In addition to the above, such purposes include: Providing our products and services to users; Auditing, research, and analysis in order to maintain, protect and improve our services; Developing new services. FORSTU processes personal information on our servers in India and in other countries. In some cases, we process personal information on a server outside your own country.
</p>
 
<h3>
Choices for personal information</h3>
<p>
When you sign up for a particular service that requires registration, we ask you to provide personal information. If we use this information in a manner different than the purpose for which it was collected, then we will ask for your consent prior to such use. If we propose to use personal information for any purposes other than those described in this Policy and/or in the specific service notices, we will offer you an effective way to opt out of the use of personal information for those other purposes. We will not collect or use information for purposes other than those described in this Policy unless we have obtained your prior consent. You can decline to submit personal information to any of our services, in which case FORSTU may not be able to provide those services to you.
</p>

<h3>
Opt-Out Policy
</h3>
<p>
FORSTU offers its visitors and customers a means to choose how we may use information provided. If, at any time after registering for information or ordering the Service, you change your mind about receiving information from us or about sharing your information with third parties, send us a request specifying your new choice. Simply send your request to info@forstu.co.
</p>
<h3>
Information sharing	
</h3>

<p>
FORSTU only shares personal information with other companies or individuals outside of FORSTU in the following limited circumstances: We have your consent. We require opt-in consent for the sharing of any sensitive personal information. We provide such information to our subsidiaries, affiliated companies or other trusted businesses or persons for the purpose of processing personal information on our behalf. We require that these parties agree to process such information based on our instructions and in compliance with this Policy and any other appropriate confidentiality and security measures. We have a good faith belief that access, use, preservation or disclosure of such information is reasonably necessary to (a) satisfy any applicable law, regulation, legal process or enforceable governmental request (b) detect, prevent, or otherwise address fraud, security or technical issues, or (c) protect against imminent harm to the rights, property or safety of FORSTU, its users or the public as required or permitted by law. If FORSTU becomes involved in a merger, acquisition, or any form of sale of some or all of its assets, we will provide notice before personal information is transferred and becomes subject to a different privacy policy. Please contact us at the address below for any additional questions about the management or use of personal data or as to what classifies as Sensitive Information.
</p>

<h3>
Information security	
</h3>
<p>

We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These include internal reviews of our data collection, storage and processing practices and security measures, as well as physical security measures to guard against unauthorized access to systems where we store personal data. We restrict access to personal information to FORSTU employees, contractors, and agents who need to know that information in order to operate, develop or improve our services. These individuals are bound by confidentiality obligations and may be subject to discipline, including termination and criminal prosecution, if they fail to meet these obligations.
</p>

<h3>
Data integrity
</h3>

<p>
FORSTU processes personal information only for the purposes for which it was collected and in accordance with this Policy or any applicable service-specific privacy notice. We review our data collection, storage, and processing practices to ensure that we only collect, store and process the personal information needed to provide or improve our services. We take reasonable steps to ensure that the personal information we process is accurate, complete, and current, but we depend on our users to update or correct their personal information whenever necessary.
</p>

<h3>
Accessing and updating personal information</h3>
<p>

When you need to change or update your personal information please mail us at info@forstu.co and we will make good faith efforts to change/correct this data if it is inaccurate or to delete such data at your request if it is not otherwise required to be retained by law or for legitimate business purposes. We may ask individual users to identify themselves and then proceed to take action as required. We may decline to process requests that are unreasonably repetitive or systematic, require disproportionate technical effort, jeopardize the privacy of others, or would be extremely impractical (for instance, requests concerning information residing on backup tapes), or for which access is not otherwise required. In any case, where we provide information access and correction, we perform this service of charge.
</p>
 
<h3>
Enforcement
</h3>
<p>
FORSTU regularly reviews its compliance with this Policy. Please feel free to direct any questions or concerns regarding this Policy or FORSTU 's treatment of personal information by emailing us at info@forstu.co . When we receive formal written complaints at this address, it is FORSTU’s policy to contact the complaining user regarding his or her concerns. We will cooperate with the appropriate regulatory authorities, including local data protection authorities, to resolve any complaints regarding the transfer of personal data that cannot be resolved between FORSTU and an individual.
</p>
 
<h3>
Changes to this policy</h3>

<p>
Please note that this Privacy Policy may change from time to time. We will not reduce your rights under this Policy without your explicit consent, and we expect most such changes will be minor. Regardless, we will post any Policy changes on this page and, if the changes are significant, we will provide a more prominent notice (including, for certain services, email notification of Policy changes).

FORSTU is a brand name registered under Dream Chasers Edutech Pvt Ltd, it’s mission is to provide IT services and fill the gaps in education industry with the use of technology.

If you have questions or concerns regarding this statement, you should first contact FORSTU team on the "contact us" page.  You may contact Dream Chasers Edutech Pvt Ltd. or email to info@forstu.co,  if your inquiry is not addressed satisfactorily,

We will serve as a liaison to work toward resolutions of your concerns.

</p>
</div>
@endsection