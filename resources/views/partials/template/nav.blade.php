
    <!-- Offcanvas Menu Begin -->
    <div class="offcanvas-menu-overlay"></div>
    <div class="offcanvas-menu-wrapper">
        
        <nav class="offcanvas__menu mobile-menu">
            <ul>
                @auth
                <li><a href="{{route('students.home')}}">View Dashboard</a></li>
                <li class="active"><a href="#">Scholarships</a>
                    <ul class="dropdown">
                        <li><a href="./services.html">Merit Based Scholarship</a></li>
                        <li><a href="./services-details.html">Income Based Scholarship</a></li>
                        <li><a href="./services.html">Sports Scholarship</a></li>
                        <li><a href="./services-details.html">Study Abroad Scholarship</a></li>
                        <li><a href="./blog-details.html">Loan Scholarships </a></li>
                    </ul>
                </li>
                
                <li><a href="{{route('achievers')}}">Scholarship Achievers</a></li>
                
                @else
                <li class="active"><a href="#">Scholarships</a>
                    <ul class="dropdown">
                        <li><a href="./services.html">Merit Based Scholarship</a></li>
                        <li><a href="./services-details.html">Income Based Scholarship</a></li>
                        <li><a href="./services.html">Sports Scholarship</a></li>
                        <li><a href="./services-details.html">Study Abroad Scholarship</a></li>
                        <li><a href="./blog-details.html">Loan Scholarships </a></li>
                    </ul>
                </li>
                
                <li><a href="{{route('achievers')}}">Scholarship Achievers</a></li>
                <li><a href="{{route('login')}}">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                Login</a></li>
                <li style="background: #88C417; padding: 11px 20px; position: relative;cursor: pointer;"><a href="{{route('register')}}">
                                Register Now</a>
                                    
                                </li>
                @endauth
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        
    </div>
    <!-- Offcanvas Menu End -->

    
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="header__logo">
                        <a href="http://www.forstu.co/"><img src="{{asset('external/img/logo3.png')}}" alt="" height="50"></a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="header__nav">
                        <nav class="header__menu">
                            <ul>
                                @auth
                                <li class="active"><a href="{{route('activescholarships')}}">Scholarships</a>
                                <ul class="dropdown">
                                    <li><a href="{{route('activescholarships')}}">All Active Scholarships</a></li>
                                <li><a href="./services.html">Merit Based Scholarship</a></li>
                                <li><a href="./services-details.html">Income Based Scholarship</a></li>
                                <li><a href="./services.html">Sports Scholarship</a></li>
                                <li><a href="./services-details.html">Study Abroad Scholarship</a></li>
                                <li><a href="./blog-details.html">Loan Scholarships </a></li>
                                </ul>
                            </li>
                            
                                <li><a href="{{route('achievers')}}">Scholarship Achievers</a></li>
                                
                                <li>
                                    @if(Auth::user()->user_type ==='student')
                                    <a href="{{route('students.home')}}">View Dashboard</a>
                                    @else
                                    <a href="{{route('admin.home')}}">View Admin Dashboard</a>
                                    @endif
                                </li>

                                
                                @else
                                <li class="active"><a href="{{route('activescholarships')}}">Scholarships</a>
                    <ul class="dropdown">
                        <li><a href="{{route('activescholarships')}}">All Active Scholarships</a></li>
                        <li><a href="{{route('activescholarships')}}">Merit Based Scholarship</a></li>
                        <li><a href="{{route('activescholarships')}}">Income Based Scholarship</a></li>
                        <li><a href="{{route('activescholarships')}}">Sports Scholarship</a></li>
                        <li><a href="{{route('activescholarships')}}">Study Abroad Scholarship</a></li>
                        <li><a href="{{route('activescholarships')}}">Loan Scholarships </a></li>
                    </ul>
                </li>
               
                
                                <li><a href="{{route('achievers')}}">Scholarship Achievers</a></li>
                                
                                
                                
                                <li><a href="{{route('login')}}">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                Login</a></li>
                                <li style="background: #88C417; padding: 11px 20px; position: relative;cursor: pointer;"><a href="{{route('register')}}" class="primary-btn"><i class="fa fa-user-plus" aria-hidden="true"></i>&nbsp;Register Now </a>
                                    
                                </li>
                                @endauth
                                
                            </ul>
                            
                        </nav>
                        
                    </div>
                </div>
            </div>
            <div class="canvas__open">
                <span class="fa fa-bars"></span>
            </div>
        </div>
    <br>
    <!-- Header Section End -->
