@extends('layouts.main')
@section('content')

<style type="text/css">
	h1 { font-family: "Segoe UI"; font-size: 41px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 45.1px; } h3 { font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 700; line-height: 15.4px; } p { font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif; font-size: 14px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 20px; } blockquote { font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif; font-size: 21px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 30px; } pre { font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans", "Helvetica Neue", Arial, sans-serif; font-size: 13px; font-style: normal; font-variant: normal; font-weight: 400; line-height: 18.5714px; }
</style>
<center><h1>About FORSTU</h1></center>

<p>
	<div class="container">
		<div class="card" style="width: 1140px;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0), 0 6px 20px 0 rgba(0, 0, 0, 0.19); border: none;">
			<div class="card-body">
				FORSTU is a platform with a mission to connect Students with different Scholarship Opportunities globally. FORSTU works as a Scholarship aggregator which helps students to search right Scholarships for them.

				Our aim is to provide support to the Scholarship Providers (Like -NGO, Corporates, Foundations, and HNI Individuals) by managing the end-to-end scholarship process with technology driven solutions in transparent and efficient manner. We aim to serve Individuals as well as organizations in designing their customized scholarship programs in alignment with their social vision.

				FORSTU also runs a virtual facilitation center through which it provides Automatic Resubmission Services for Students.
			</div>
		</div>
		
	</div>
	

</p>
	<br>
	
	<br>


<div class="container">
		
<center><h1 style="text-align: left;"> Scholarship Facilitation Centre</h1></center>
<p>
	<div class="row">
		<div class="col-md-4">
			<div class="card" style="width: 250px;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0), 0 6px 20px 0 rgba(0, 0, 0, 0.19); border: none;">
						<div class="card-body">
							Scholarship Facilitation Centre is an online place for the private as well as government scholarahip application procedures for every student. It introduces and enables effectivity towards the scholarahip application procedures, resulting in efficient utilisation of CSR funds.

							<a href="" class="btn btn-primary btn-xs" style="text-align: right; margin-top: 7px;" >How to Register</a>
			</div>
		</div>
			
	</div>
	<div class="col-md-8">
		<img src="{{asset('external/img/b.png')}}" width="100%">
        
	</div>
	</div>

				
</p>
</div>


@endsection