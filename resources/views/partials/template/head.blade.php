<meta charset="UTF-8">
    <meta name="description" content="FORSTU Scholarships">
    <meta name="keywords" content="Scholarships, FORSTU, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700;900&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
<script src="https://use.fontawesome.com/0b0c8d0220.js"></script>
    

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('external/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('external/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('external/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('external/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('external/css/magnific-popup.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('external/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('external/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('external/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('external/css/style.css')}}" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
   
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
