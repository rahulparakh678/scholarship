<!-- Loan Services Section Begin -->
    <section class="loan-services spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>How we Work</h2>
                        <h3>Register Once & Get Applied Everywhere</h3><br>
                        <h5>We dont let you miss any eligible scholarship</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="loan__services__list">
                <div class="loan__services__item set-bg" data-setbg="{{asset('external/img/loan-services/ls-1.jpg')}}">     
                    
                    <div class="loan__services__item__text">

                        <h4><span>Step 1.</span><br> Register & Create Profile</h4>
                        <p>Register on our website and fill all your educational details under Profile Section</p>
                        
                    </div>
                </div>
                <div class="loan__services__item set-bg" data-setbg="{{asset('external/img/loan-services/ls-4.jpg')}}">
                    <div class="loan__services__item__text">
                        <h4><span>Step 2.</span><br> Complete Payment</h4>
                        <p>Pay your Registration Fees INR 1500 after Completion of the Profile.</p>
                        
                    </div>
                </div>
                <div class="loan__services__item set-bg" data-setbg="{{asset('external/img/loan-services/ls-3.jpg')}}">
                    <div class="loan__services__item__text">
                        <h4><span>Step 3.</span><br> Scholarship Application</h4>
                        <p>We will forward your application eligible scholarships and  screenshot will be sent.</p>
                        
                    </div>
                </div>
                <div class="loan__services__item set-bg" data-setbg="{{asset('external/img/loan-services/ls-5.jpg')}}">
                    <div class="loan__services__item__text">
                        <h4><span>Step 4.</span><br> Verification</h4>
                        <p>Your profiles and documents provided will be verified by Scholarship Providers</p>
                        
                    </div>
                </div>
                <div class="loan__services__item set-bg" data-setbg="{{asset(
                'external/img/loan-services/ls-2.jpg')}}">
                    <div class="loan__services__item__text">
                        <h4><span>Step 5.</span><br>Results </h4>
                        <p>Scholarship will be given to the students selected by Providers</p>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Loan Services Section End -->
