
    <!-- Footer Section Begin -->
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__logo">
                            <h2 style="font-size: 40px;font-weight: 700;color: #88C417;margin-top: 10px;">FORSTU</h2>
                        </div>
                        <p>Transactional Market Place between Scholarship Providers and Scholarship Seekers</p>
                    </div>
                </div>
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                    <div class="footer__widget footer__widget--address">
                        <h5>Social Media</h5>
                        
                        <ul>
                            <li><span><a href="https://www.facebook.com/FORSTUOFFICIAL/" target="_blank"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i>&nbsp;</a></span><a href="https://www.instagram.com/forstuofficial/" target="_blank"><i class="fa fa-instagram fa-lg"></i>&nbsp;</a><a href="https://www.youtube.com/channel/UCmpYESe5t3ICN6EOTRcBplw" target="_blank"><i class="fa fa-youtube fa-lg"></i>&nbsp;</a><a href="https://www.linkedin.com/company/forstu-edutech" target="_blank"><i class="fa fa-linkedin fa-lg"></i>&nbsp; </a><a href="https://wa.me/917757039556/?text=Hello%20I%20want%20to%20know%20more%20about%20FORSTU" target="_blank"><span><i class="fa fa-whatsapp fa-lg" aria-hidden="true"></i>&nbsp;</span></a></li>
                            
                            
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-3 offset-lg-1 col-md-6 col-sm-6">
                    <div class="footer__widget footer__widget--address">
                        <h5>Contact Us on</h5>
                        
                        <ul>
                            <li><span><i class="fa fa-envelope-open"></i> &nbsp;&nbsp;info@forstu.co </span></li>
                            <li><i class="fa fa-phone"></i>&nbsp;&nbsp;7757039556 / 7841816646</li>

                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer__copyright">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <ul class="footer__copyright__links">
                            <li><a href="{{route('faq')}}">FAQ</a></li>
                            <li><a href="{{route('terms')}}">Terms & Conditions</a></li>
                            <li><a href="{{route('privacy')}}">Privacy Policy</a></li>
                            <li><a href="{{route('disclaimer')}}">Disclaimer</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        <a href='https://www.symptoma.com/en/info/covid-19'>Corona Virus first Symptoms</a> <script type='text/javascript' src='https://www.freevisitorcounters.com/auth.php?id=925fb8b70d9671cf27f4669f84eadf55f4696eb4'></script>
                        <script type="text/javascript" src="https://www.freevisitorcounters.com/en/home/counter/725290/t/5"></script>
                        <div class="footer__copyright__text">
                            <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Made in <i class="fa fa-heart" aria-hidden="true"></i> by <a href="" target="_blank">FORSTU</a></p>
                        </div>

                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </div>
                </div>
            </div>
        </div>
       <center>
           <span id="siteseal"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=OOfF5sRDqCUm8sdKRJWdSuYlKLQ6cENJgRsQcCGkkWoOUxhepp0r69TtvM3o"></script></span>
       </center> 
    </footer>
    <!-- Footer Section End -->

    
    <!-- Js Plugins -->
    <script src="{{asset('external/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('external/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('external/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('external/js/jquery-ui.min.js')}}"></script>
    <script src="{{asset('external/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('external/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('external/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('external/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('external/js/main.js')}}"></script>


<!-- Go to www.addthis.com/dashboard to customize your tools -->
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f21e3184b0c3cd9"></script>
