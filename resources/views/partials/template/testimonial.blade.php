<!-- Testimonial Section Begin -->
    <div class="testimonial spad set-bg" data-setbg="{{asset('external/img/testimonial/testimonial-bg.jpg')}}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>What Students Are Saying</h2>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="testimonial__carousel">
        <div class="container">
            <div class="row">
                <div class="testimonial__slider owl-carousel">
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('external/img/testimonial/archana.jpeg')}}" alt="" style="vertical-align: middle;width: 100px;height: 100px;border-radius: 50%;">
                            <h5>Archana Mahadev Rodge</h5>
                            <span>Magma Fincorp Foundation</span>
                            <p>I am doing MBBS at Grant Government Medical college, Mumbai. 
                                <strong>FORSTU helped me to get magma foundation scholarships of Rs 2 Lakh</strong>. FORSTU is really helping needy students.</p>
                        </div>
                    </div>
                    
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('external/img/testimonial/bhavesh.jpeg')}}" alt="" style="vertical-align: middle;width: 100px;height: 100px;border-radius: 50%;">
                            <h5>Bhavesh Poladiya</h5>
                            <span>C.B Chajjed Scholarship</span>
                            <p>I am studying M.Pharmacy at R.C Patel College. 
                                <strong>FORSTU helped me to receive 7500Rs scholarship from C.B Chajjed  Trust</strong>. Thank You FORSTU for helping me with scholarship in my hard times</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('external/img/testimonial/priyanka.jpeg')}}" alt="" style="vertical-align: middle;width: 100px;height: 100px;border-radius: 50%;">
                            <h5>Priyanka Jadhav</h5>
                            <span>TransUnion Cibil Scholarship</span>
                            <p>I am studying at Dr.D Y Patil College Of Engineering . When I was in 7th standard my father passed away .<strong>Because of FORSTU i got TransUnion Cibil  scholarship worth Rs 40000 for my engineering courses </strong>
</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('external/img/testimonial/riddhesh.jpg')}}" alt="" style="vertical-align: middle;width: 100px;height: 100px;border-radius: 50%;">
                            <h5>Riddhesh Pagariya</h5>
                            <span>India Bulls Foundation Scholarship</span>
                            <p>I am studying B.Pharmacy <strong>FORSTU helped for India Bulls Foundation Scholarship. I am recieving Rs 92000 Scholarship from last two years.</strong>.FORSTU Send timely alerts & notifications ofscholarships.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('external/img/testimonial/vaishnavi1.jpeg')}}" alt="" style="vertical-align: middle;width: 100px;height: 100px;border-radius: 50%;">
                            <h5>Vaishnavi Khosare</h5>
                            <span>TransUnion Cibil Scholarship</span>
                            <p>I'm studying civil engineering at D.Y Patil College.
                            <strong>I got 40000 rupees by transunion cibil scholarship and that's very very helpful for me. </strong>Through this This helped me to pay me college fees. </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('external/img/testimonial/aditya.jpeg')}}" alt="" style="vertical-align: middle;width: 100px;height: 100px;border-radius: 50%;">
                            <h5>Aditya Dharamshi</h5>
                            <span>HDFC Educational Crisis Scholarship</span>
                            <p>ForStu is one of the platform with very kind team.<strong>FORSTU helped me with scholarship of ₹25,000 from HDFC, right from documentation to interview, and that too right from my home.</strong> </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('external/img/testimonial/ismail.jpeg')}}" alt="" style="vertical-align: middle;width: 100px;height: 100px;border-radius: 50%;">
                            <h5>Ismail Shaikh</h5>
                            <span>HDFC Educational Crisis Scholarship</span>
                            <p>I am pursuing my PharmD graduation from Nanded.<strong>I got HDFC Education crises scholarship of Rs 25000 .</strong>Thank u very much from my bottom of 💓 and really appreciate your great job and for help.</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="testimonial__item">
                            <img src="{{asset('external/img/testimonial/vinaya.jpg')}}" alt="" style="vertical-align: middle;width: 100px;height: 100px;border-radius: 50%;">
                            <h5>Vinaya Katole</h5>
                            <span>HDFC Educational Crisis Scholarship</span>
                            <p> My father & grandfather was expired in few months in fatal accident.<strong> . I got scholarship HDFC crisis education scholarship (Rs 25000).  </strong> FORSTU Provided me 24*7 Support </p>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Testimonial Section End -->
