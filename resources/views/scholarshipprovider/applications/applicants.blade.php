@extends('layouts.scholarshipprovider')
@section('content')

<div class="container">
<div class="row justify-content-center">

  <div class="col-md-3">
    <div class="alert alert-primary" role="alert">
     <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  Application Count</a> 
     
     <center><h1><a href="#" class="alert-link"><?php
                                                   $count=sizeof($results);
                                                   echo "<center><h1>$count</h1></center>";
                                                  ?></a></h1>
      </center>
    </div>
    </div>
     <div class="col-md-3">
      <div class="alert alert-primary" role="alert">
      <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  Shortlisted Candidates</a> 

       <center><h1><a href="#" class="alert-link"> <?php
     $count2=sizeof($shortlist);
     echo "<center><h1>$count2</h1></center>";
     ?></a></h1>
      </center>
     
     </div>
    </div>
    <div class="col-md-3">
      <div class="alert alert-primary" role="alert">
      <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  Awarded Candidates</a> 
      <center><h1><a href="#" class="alert-link"> <?php
      global $count1;
     $count1=sizeof($awarded);
     echo "<center><h1>$count1</h1></center>";
     ?> </a></h1>
      </center>

     
     </div>
     
    </div>
    <div class="col-md-3">
      <div class="alert alert-primary" role="alert">
      <a href="#" class="alert-link"><i class="fa fa-users fa-lg" aria-hidden="true"></i> &nbsp;  Corpus Available</a> 
      
      <?php
      global $corpus;
      global $amt;
      $amt=$scholarship->scholarship_amount;
      $corpus=$scholarship->scholarship_corpus;
      $result=$corpus-($count1*$amt);
      ?>

        <center><h1><a href="#" class="alert-link">{{$result ?? ''}}</a></h1></center> 
     </div>
    
</div>
</div>
<div class="row">
  <div class="col-md-3">
    
    <div class="alert alert-primary" role="alert">
       <a href="#" class="alert-link"><i class="fa fa-male fa-3x  " aria-hidden="true"></i> &nbsp; Male</a> 
         <center><h1><a href="#" class="alert-link">{{$male ?? ''}}</a></h1></center>
    </div>
    
    </div>
      <div class="col-md-3">
        <div class="alert alert-primary" role="alert">
          <a href="#" class="alert-link"><i class="fa fa-female fa-3x  " aria-hidden="true"></i> &nbsp; Female</a> 
         <a> <center><h1><a href="#" class="alert-link">{{$female ?? ''}}</a></h1></center></a>
        </div>
      </div>
       <div class="col-md-3">
        <div class="alert alert-primary" role="alert">
          <a href="#" class="alert-link"><i class="fa fa-wheelchair fa-3x" aria-hidden="true"></i> &nbsp; Physically Handicapped</a> 
         <a><center><h1><a href="#" class="alert-link">{{$handicapped ?? ''}}</a></h1></center></a>
        </div>
      </div>
       <div class="col-md-3">
        <div class="alert alert-primary" role="alert">
          <a href="#" class="alert-link"><i class="fa fa-users fa-3x" aria-hidden="true"></i> &nbsp; Single Parent</a> 
          <a><center><h1><a href="#" class="alert-link">{{$single_parent ?? ''}}</a></h1></center></a>
        </div>
      </div>
</div>
</div>
<div class="row">
<div class="container">
  
      <div class="card">
        <div class="card-header">
            {{$scholarship->scheme_name}} Scholarship Applications
        </div>
        <div class="card-body">
          <div class="row">
            &nbsp;<a href="{{route('filteredview',$scholarship->id)}}" class="btn btn-primary">Apply Filters</a>
            
            <br>
            <br>
          </div>
          <div class="table-responsive">
            <table class=" table table-bordered  table-hover datatable datatable-Setupscholarship">
              <thead>
                  <tr>
                   <th></th>
                    <th>Student Name</th>
                    <th>Current Status</th>
                    <th>Action</th>
                        
                  </tr>

              </thead>
              <tbody>

                @foreach($results as $result)
                 <tr>
                   <td></td>
                   <td>
                    <?php
                    $profiles=App\StudentProfile::where('user_id',$result->user_id)->first();
                    echo $profiles->dob;
                    ?>
                    {{$result->user_name ?? ''}}
                      

                     <a href="{{ route('showprofile', $result->user_id) }}" style="text-decoration: none;" target="_blank"> <i class="fa fa-eye fa-2x" aria-hidden="true" style="float: right; text-decoration: none;"></i></a>
                   </td>
                   <td>
                     <span class="badge badge-warning">{{$result->status ?? ''}}</span>
                   </td>
                   <td>
                     
                                    
                                    
                                    <a href="{{route('Shortlised',$result->id)}}" class="btn btn-xs btn-dark">Shortlist</a>
                                    <a href="{{route('Rejected',$result->id)}}" class="btn btn-xs btn-danger">Better Luck Next Time</a>
                                    <a href="{{route('Awarded',$result->id)}}" class="btn btn-xs btn-success">Award Scholarship</a>

                                   
                   </td>
                   
                 </tr>
                @endforeach
              </tbody>
                            
            </table>
           </div> 
          
        </div>

      </div>
    
</div>
 </div>

@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

  

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 10,
  });
  let table = $('.datatable-Setupscholarship:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  
})

</script>
@endsection