@extends('layouts.scholarshipprovider')
@section('content')
<div class="content">
    <div class="form-group">
                <a class="btn btn-primary" href="{{ URL::previous() }}">
                    Back
                </a>
                
                
                
                
                <div style="float: right;">
                       <a href="#" class="btn btn-xs btn-dark">Shortlist</a>
                                    <a href="#" class="btn btn-xs btn-danger">Better Luck Next Time</a>
                                    <a href="#" class="btn btn-xs btn-success">Award Scholarship</a>
                </div>
                
                
    </div>
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    Personal Details
                </div>
                <div class="card-body">
                    <img src="{{ asset($profile->photo ) }}" width="150" style="display: block;margin-left: auto;margin-right: auto;">
                    <hr><strong>Full Name:</strong> {{$profile->fullname}}
                    <hr><strong>Date of Birth:</strong> {{$profile->dob}}
                    @if(!empty($profile->domicile_certificate))
                    <a href="{{$profile->domicile_certificate}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                    
                    <hr><strong>Gender:</strong> {{$profile->gender}}
                    <hr><strong>Email:</strong> {{$profile->email}}
                    <hr><strong>Mobile:</strong> {{$profile->mobile}}
                    <hr><strong>Religion:</strong> {{$profile->religion}}

                    <hr><strong>Caste</strong> {{$profile->caste->caste_name}}

                    @if(!empty($profile->caste_certificate))
                    <a href="{{$profile->caste_certificate}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif

                    <hr><strong>Marital Status:</strong> {{$profile->marital_status}}
                    <hr><strong>Single Parent:</strong> {{$profile->single_parent}}
                    @if(!empty($profile->death_certificate))
                    <a href="{{$profile->death_certificate}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif

                    <hr><strong>Physically handicapped:</strong> {{$profile->handicapped}}
                    @if(!empty($profile->physically_handicapped_certificate))
                    <a href="{{$profile->physically_handicapped_certificate}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif

                    <hr><strong>Aadhar Number</strong> {{$profile->aadharnumber}} &nbsp; &nbsp; 

                    @if(!empty($profile->aadhar_card))
                    <a href="{{$profile->aadhar_card}}" style="text-decoration: none;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif



                </div>
            </div>
         </div>
        
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Family Details
                </div>
                <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <strong>Father Name:</strong> {{$profile->father_name}}
                                </div>
                                <div class="col-md-4">
                                    <strong>Father Education</strong> {{$profile->father_edu}}
                                </div>
                                <div class="col-md-4">
                                    <strong>Father occupation:</strong> {{$profile->father_occupation}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-4">
                                    <strong>Mother Name:</strong> {{$profile->mother_name}}
                                </div>
                                <div class="col-md-4">
                                    <strong>Mother Education</strong> {{$profile->mother_edu}}
                                </div>
                                <div class="col-md-4">
                                    <strong>Mother occupation:</strong> {{$profile->mothers_occupation}}
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                    <strong>Parents Contact Number</strong> {{$profile->parents_mobile}}
                                </div>
                                <div class="col-md-6">
                                    <strong>Parents Annual Income</strong> {{$profile->annual_income}}
                                    @if(!empty($profile->income_certificate))
                    <a href="{{$profile->income_certificate}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                    
                                </div>
                                
                            </div>      
                                        
                    </div>
                </div>

            

            
            <div class="row">
                 <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Current Course Details
                        </div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-md-4">
                                    <strong>Current Course:</strong> {{$profile->course_name->course_name ?? ''}}
                                </div>
                                <div class="col-md-4">
                                     <strong>Current Year:</strong> {{$profile->current_year}}
                                </div>
                                <div class="col-md-4">
                                     <strong>Course Pattern:</strong> {{$profile->course_pattern}}
                                     @if(!empty($profile->clg_id_card))
                    <a href="{{$profile->clg_id_card}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                    
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            <hr>
                            <div class="row">
                                
                                <div class="col-md-12">
                                    <strong>Current Institute</strong> {{$profile->current_inst_name}}<br>{{$profile->inst_address}}
                                    @if(!empty($profile->admission_letter))
                    <a href="{{$profile->admission_letter}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                                </div>
                                
                            </div>
                            
                            
                            <hr>
                            
                            <div class="row">
                                <div class="col-md-4">
                                    <strong>Tution Fees:</strong> {{$profile->tution_fees}}
                                    @if(!empty($profile->currentyear_fees_reciept))
                    <a href="{{$profile->currentyear_fees_reciept}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                                </div>
                                <div class="col-md-4">
                                    <strong>Non Tution Fees:</strong> {{$profile->non_tution_fees}}
                                </div>
                                <div class="col-md-4">
                                    <strong>Hostel Fees:</strong> {{$profile->hostel_fees}}
                                    @if(!empty($profile->hostel_reciept))
                    <a href="{{$profile->hostel_reciept}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                                </div>
                            </div>
                            

                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                 <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Educational Details
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                
                            
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                    <th>Course</th>
                                    <th>Institute Name</th>
                                    <th>State</th>
                                    <th>Passing Year</th>
                                    <th>Percentage</th>
                                </thead>
                                <tbody>
                                  
                                  @if(!empty($profile->class_10_school_name))
                                    <tr>
                                        <td style="background-color: #2f353a;border-color: #40484f;color: #fff;font-weight: bold;">Class 10</td>
                                        <td>{{$profile->class_10_school_name}}</td>
                                        <td>{{$profile->class_10_state}}</td>
                                        <td>{{$profile->school_passing}}</td>
                                        <td>{{$profile->school_percentage}}
                                            @if(!empty($profile->class10_marksheet))
                    <a href="{{$profile->class10_marksheet}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                                        </td>


                                    </tr>
                                    @endif

                                    @if(!empty($profile->class_12_clg_name))
                                    <tr>
                                        <td style="background-color: #2f353a;border-color: #40484f;color: #fff;font-weight: bold;">Class 12</td>
                                        <td>{{$profile->class_12_clg_name}}</td>
                                        <td>{{$profile->class_12_state}}</td>
                                        <td>{{$profile->class_12_passing_yeat}}</td>
                                        <td>{{$profile->class_12_percentage}}
                                            @if(!empty($profile->class12_marksheet))
                    <a href="{{$profile->class12_marksheet}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                                        </td>
                                    </tr>
                                    @endif

                                    @if(!empty($profile->diploma_clg_name))
                                    <tr>
                                        <td style="background-color: #2f353a;border-color: #40484f;color: #fff;font-weight: bold;">Diploma Details</td>
                                        <td>{{$profile->diploma_clg_name}}</td>
                                        <td>{{$profile->diploma_state}}</td>
                                        <td>{{$profile->diploma_passing_year}}</td>
                                        <td>{{$profile->diploma_percentage}}
                                            @if(!empty($profile->diploma_marksheet))
                    <a href="{{$profile->diploma_marksheet}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                                        </td>
                                    </tr>
                                    @endif

                                    @if(!empty($profile->grad_clg_name))
                                    <tr>
                                        <td style="background-color: #2f353a;border-color: #40484f;color: #fff;font-weight: bold;">Graduation Details</td>
                                        <td>{{$profile->grad_clg_name}}</td>
                                        <td>{{$profile->grad_state}}</td>
                                        <td>{{$profile->grad_passing_year}}</td>
                                        <td>{{$profile->grad_percentage}}
                                            @if(!empty($profile->graduation_marksheet))
                    <a href="{{$profile->graduation_marksheet}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                    @endif
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>




        </div>

        <div class="col-md-3">
            <div class="row">
                <div class="card">
                    <div class="card-header">
                        Communication Details
                    
                    </div>
                    <div class="card-body">
                        <strong>Current Address :</strong> {{$profile->current_add}} &nbsp {{$profile->pincode}}<br>
                        <strong>Current City:</strong> {{$profile->current_city}}<br>
                        <strong>Current State:</strong> {{$profile->current_state}}
                        <hr>
                        <strong>Permanent Address :</strong> {{$profile->permanent_add}} &nbsp {{$profile->permanent_pincode}}<br>
                        <strong>Permanent City:</strong> {{$profile->permanent_city}}<br>
                        <strong>Permanent State:</strong> {{$profile->permanent_state}}
                        <hr>

                        @if(!empty($profile->address_proof))
                        <a href="{{$profile->address_proof}}" style="text-decoration: none; float: right;" target="_blank"><i class="fa fa-file-pdf-o fa-2x" aria-hidden="true"></i></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
@parent

@endsection